/*
 * Copyright 2010 Matteo Bruni for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#include "config.h"
#include "wine/port.h"

#include "d3d10core_private.h"

WINE_DEFAULT_DEBUG_CHANNEL(d3d10core);

/* IUnknown methods */

static HRESULT WINAPI d3d10_blob_QueryInterface(ID3D10Blob *iface, REFIID riid, LPVOID* ppobj)
{
    if (IsEqualGUID(riid, &IID_IUnknown) ||
        IsEqualGUID(riid, &IID_ID3D10Blob))
    {
        IUnknown_AddRef(iface);
        *ppobj = iface;
        return S_OK;
    }

    WARN("Interface %s not implemented\n", debugstr_guid(riid));
    *ppobj = NULL;
    return E_NOINTERFACE;
}

static ULONG WINAPI d3d10_blob_AddRef(ID3D10Blob *iface)
{
    struct d3d10_blob *This = (struct d3d10_blob *)iface;
    ULONG refcount = InterlockedIncrement(&This->refcount);

    TRACE("%p increasing refcount to %u\n", This, refcount);

    return refcount;
}

static ULONG WINAPI d3d10_blob_Release(ID3D10Blob *iface)
{
    struct d3d10_blob *This = (struct d3d10_blob *)iface;
    ULONG refcount = InterlockedDecrement(&This->refcount);

    TRACE("%p decreasing refcount to %u\n", This, refcount);

    if (!refcount)
    {
        HeapFree(GetProcessHeap(), 0, This->buffer);
        HeapFree(GetProcessHeap(), 0, This);
    }
    return refcount;
}

/* ID3D10Blob methods */
static LPVOID WINAPI d3d10_blob_GetBufferPointer(ID3D10Blob *iface)
{
    struct d3d10_blob *This = (struct d3d10_blob *)iface;
    return This->buffer;
}

static DWORD WINAPI d3d10_blob_GetBufferSize(ID3D10Blob *iface)
{
    struct d3d10_blob *This = (struct d3d10_blob *)iface;
    return This->size;
}

const struct ID3D10BlobVtbl d3d10_blob_vtbl =
{
    d3d10_blob_QueryInterface,
    d3d10_blob_AddRef,
    d3d10_blob_Release,
    d3d10_blob_GetBufferPointer,
    d3d10_blob_GetBufferSize
};

HRESULT WINAPI D3D10CreateBlob(SIZE_T size, ID3D10Blob **blob)
{
    struct d3d10_blob *obj;

    obj = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(*obj));
    if (obj == NULL)
    {
        *blob = NULL;
        return E_OUTOFMEMORY;
    }
    obj->vtbl = &d3d10_blob_vtbl;
    obj->refcount = 1;
    obj->size = size;
    obj->buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, size);
    if (obj->buffer == NULL)
    {
        HeapFree(GetProcessHeap(), 0, obj);
        *blob = NULL;
        return E_OUTOFMEMORY;
    }

    *blob = (ID3D10Blob *)obj;
    return S_OK;
}
