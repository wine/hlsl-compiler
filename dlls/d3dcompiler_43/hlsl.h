/*
* Direct3D HLSL compiler
*
* Copyright 2010 Matijn Woudt
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
*/

#include "d3dcompiler_43_private.h"

#define MESSAGEBUFFER_INITIAL_SIZE 256

/* hlsl_ctx is defined here because it is needed by the parser and the lexer */
struct hlsl_parser hlsl_ctx;

struct hlsl_variable;

struct hlsl_struct {
    struct hlsl_variable *members;
    int count;
};

enum hlsl_intrinsic_functions {
    F_ABS,
    F_ACOS,
    F_ALL,
    F_ALLMEMORYBARRIER,
    F_ALLMEMORYBARRIERWITHGROUPSYNC,
    F_ANY,
    F_ASDOUBLE,
    F_ASFLOAT,
    F_ASIN,
    F_ASINT,
    F_ASUINT,
    F_ATAN,
    F_ATAN2,
    F_CEIL,
    F_CLAMP,
    F_CLIP,
    F_COS,
    F_COSH,
    F_COUNTBITS,
    F_CROSS,
    F_D3DCOLORTOUBYTE4,
    F_DDX,
    F_DDX_COARSE,
    F_DDX_FINE,
    F_DDY,
    F_DDY_COARSE,
    F_DDY_FINE,
    F_DEGREES,
    F_DETERMINANT,
    F_DEVICEMEMORYBARRIER,
    F_DEVICEMEMORYBARRIERWITHGROUPSYNC,
    F_DISTANCE,
    F_DOT,
    F_DST,
    F_EVALUATEATTRIBUTEATCENTROID,
    F_EVALUATEATTRIBUTEATSAMPLE,
    F_EVALUATEATTRIBUTESNAPPED,
    F_EXP,
    F_EXP2,
    F_F16TOF32,
    F_F32TOF16,
    F_FACEFORWARD,
    F_FIRSTBITHIGH,
    F_FIRSTBITLOW,
    F_FLOOR,
    F_FMOD,
    F_FRAC,
    F_FREXP,
    F_FWIDTH,
    F_GETRENDERTARGETSAMPLECOUNT,
    F_GETRENDERTARGETSAMPLEPOSITION,
    F_GROUPMEMORYBARRIER,
    F_GROUPMEMORYBARRIERWITHGROUPSYNC,
    F_INTERLOCKEDADD,
    F_INTERLOCKEDAND,
    F_INTERLOCKEDCOMPAREEXCHANGE,
    F_INTERLOCKEDCOMPARESTORE,
    F_INTERLOCKEDEXCHANGE,
    F_INTERLOCKEDMAX,
    F_INTERLOCKEDMIN,
    F_INTERLOCKEDOR,
    F_INTERLOCKEDXOR,
    F_ISFINITE,
    F_ISINF,
    F_ISNAN,
    F_LDEXP,
    F_LENGTH,
    F_LERP,
    F_LIT,
    F_LOG,
    F_LOG10,
    F_LOG2,
    F_MAD,
    F_MAX,
    F_MIN,
    F_MODF,
    F_MUL,
    F_NOISE,
    F_NORMALIZE,
    F_POW,
    F_PROCESS2DQUADTESSFACTORSAVG,
    F_PROCESS2DQUADTESSFACTORSMAX,
    F_PROCESS2DQUADTESSFACTORSMIN,
    F_PROCESSISOLINETESSFACTORS,
    F_PROCESSQUADTESSFACTORSAVG,
    F_PROCESSQUADTESSFACTORSMAX,
    F_PROCESSQUADTESSFACTORSMIN,
    F_PROCESSTRITESSFACTORSAVG,
    F_PROCESSTRITESSFACTORSMAX,
    F_PROCESSTRITESSFACTORSMIN,
    F_RADIANS,
    F_RCP,
    F_REFLECT,
    F_REFRACT,
    F_REVERSEBITS,
    F_ROUND,
    F_RSQRT,
    F_SATURATE,
    F_SIGN,
    F_SIN,
    F_SINCOS,
    F_SINH,
    F_SMOOTHSTEP,
    F_SQRT,
    F_STEP,
    F_TAN,
    F_TANH,
    F_TEX1D,
    F_TEX1DBIAS,
    F_TEX1DGRAD,
    F_TEX1DLOD,
    F_TEX1DPROJ,
    F_TEX2D,
    F_TEX2DBIAS,
    F_TEX2DGRAD,
    F_TEX2DLOD,
    F_TEX2DPROJ,
    F_TEX3D,
    F_TEX3DBIAS,
    F_TEX3DGRAD,
    F_TEX3DLOD,
    F_TEX3DPROJ,
    F_TEXCUBE,
    F_TEXCUBEBIAS,
    F_TEXCUBEGRAD,
    F_TEXCUBELOD,
    F_TEXCUBEPROJ,
    F_TRANSPOSE,
    F_TRUNC
};

struct hlsl_data_type {
    enum hlsl_data_type_kind {
        HLSL_SCALAR_UNK,
        HLSL_SCALAR_BOOL,
        HLSL_SCALAR_DOUBLE,
        HLSL_SCALAR_FLOAT,
        HLSL_SCALAR_HALF,
        HLSL_SCALAR_INT,
        HLSL_SCALAR_STRING,
        HLSL_SCALAR_UINT,
        HLSL_STRUCT
    } type;
    int rows;
    int cols;

    enum {
        HLSL_VALUE_INVALID,
        HLSL_VALUE_IMMEDIATE,
        HLSL_VALUE_REGISTER,
        HLSL_VALUE_SEMANTIC
    } value_type;
    union {
        struct shader_reg *reg;
        struct hlsl_semantic *sem;
        struct hlsl_struct structure;
        BOOL **b;
        double **d;
        float **f;
        /* Half is stored in f */
        int **i;
        char ***s;
        unsigned int **u;
    } value;
};

struct hlsl_parameter {
    struct hlsl_parameter *next;
    struct hlsl_expression *value;
};

struct hlsl_variable {
    const char *name;
    struct hlsl_data_type *data_type;
};

struct hlsl_custom_type {
    const char *custom_name;
    struct hlsl_data_type *value;
};

struct hlsl_function {
    const char *name;
    struct hlsl_data_type *return_type;
    struct hlsl_scope *scope;
};

struct hlsl_semantic {
    enum hlsl_semantics {
        /* This one is identical to BWRITERDECLUSAGE, except for NONE, VFACE and VPOS */
        HLSL_SEM_POSITION,
        HLSL_SEM_BLENDWEIGHT,
        HLSL_SEM_BLENDINDICES,
        HLSL_SEM_NORMAL,
        HLSL_SEM_PSIZE,
        HLSL_SEM_TEXCOORD,
        HLSL_SEM_TANGENT,
        HLSL_SEM_BINORMAL,
        HLSL_SEM_TESSFACTOR,
        HLSL_SEM_POSITIONT,
        HLSL_SEM_COLOR,
        HLSL_SEM_FOG,
        HLSL_SEM_DEPTH,
        HLSL_SEM_SAMPLE,
        HLSL_SEM_NONE,
        HLSL_SEM_VFACE,
        HLSL_SEM_VPOS
    } semantic;
    unsigned int num;
};

struct hlsl_expression {
    enum hlsl_operator {
        HLSL_OP_CONST,
        HLSL_OP_VAR,
        HLSL_OP_VAR_INC,
        HLSL_OP_VAR_DEC,
        HLSL_OP_CAST,
        HLSL_OP_LOGICAL_NOT,
        HLSL_OP_UNARY_NOT,
        HLSL_OP_NEGATIVE,
        HLSL_OP_POSITIVE,
        HLSL_OP_PRE_INC,
        HLSL_OP_PRE_DEC,
        HLSL_OP_MUL,
        HLSL_OP_DIV,
        HLSL_OP_MOD,
        HLSL_OP_ADD,
        HLSL_OP_SUB,
        HLSL_OP_SHIFT_LEFT,
        HLSL_OP_SHIFT_RIGHT,
        HLSL_OP_LESS,
        HLSL_OP_LESS_EQUAL,
        HLSL_OP_GREATER,
        HLSL_OP_GREATER_EQUAL,
        HLSL_OP_EQUAL,
        HLSL_OP_NOT_EQUAL,
        HLSL_OP_BITW_AND,
        HLSL_OP_BITW_XOR,
        HLSL_OP_BITW_OR,
        HLSL_OP_AND,
        HLSL_OP_OR,
        HLSL_OP_CONDITIONAL,
        HLSL_OP_ASSIGN,
        HLSL_OP_ASSIGN_ADD,
        HLSL_OP_ASSIGN_SUB,
        HLSL_OP_ASSIGN_MUL,
        HLSL_OP_ASSIGN_DIV,
        HLSL_OP_ASSIGN_MOD,
        HLSL_OP_ASSIGN_SHIFT_LEFT,
        HLSL_OP_ASSIGN_SHIFT_RIGHT,
        HLSL_OP_ASSIGN_BITW_AND,
        HLSL_OP_ASSIGN_BITW_XOR,
        HLSL_OP_ASSIGN_BITW_OR,
        HLSL_OP_ASSIGN_PARAM,
        HLSL_OP_INTRINSIC_FUNC,
    } op;
    union {
        struct hlsl_expression *expr;
        struct hlsl_data_type *data_type;
        enum hlsl_intrinsic_functions func;
        struct hlsl_variable *var;
    } A;
    union {
        struct hlsl_expression *expr;
        struct hlsl_parameter *params;
    } B;
    struct hlsl_expression *C;
};

struct hlsl_decl {
    struct shader_reg *reg;
    struct hlsl_semantic *sem;
};

struct hlsl_instruction {
    struct hlsl_instruction *next;

    enum hlsl_instruction_type {
        HLSL_INSTR_STATEMENT,
        HLSL_INSTR_RETURN,
        HLSL_INSTR_IF,
        HLSL_INSTR_LOOP
    } type;
    struct hlsl_parameter *args;
};

struct hlsl_scope {
    struct hlsl_scope *upper;

    struct hlsl_instruction *first_instr;
    struct hlsl_instruction *last_instr;

    struct hlsl_variable **vars;
    int var_count;

    struct hlsl_custom_type **custom_types;
    int custom_type_count;

   /*struct asm_parser asm_ctx;

    struct shader_reg *return_reg;
    struct hlsl_data_type *return_type;*/
};

struct float4 {
    struct floatval {
        float v;
        BOOL used;
    } x;
    struct floatval y;
    struct floatval z;
    struct floatval w;
};

struct hlsl_parser {
    struct hlsl_scope *current_scope;
    struct hlsl_function **functions;
    unsigned int function_count;
    const char *main_func;
    const char *target;

    struct float4 **constants;
    unsigned int constant_count;

    struct hlsl_decl **inputs;
    unsigned int output_count;
    unsigned int input_count;
    unsigned int temp_count;

    struct hlsl_decl *output_decl;
    unsigned int output_decl_count;

    unsigned int line_no;
    enum parse_status /*{
        PARSE_ERR,
        PARSE_WARN,
        PARSE_SUCCESS
    } */status;
    unsigned int messagesize;
    unsigned int messagecapacity;
    char *messages;
    struct bwriter_shader *shader;
};

char *debugstr_data_type(struct hlsl_data_type *dt);
char *debugstr_params(struct hlsl_parameter *params);
char *debugstr_expression(struct hlsl_expression *expr);
const char *debugstr_intrinsic_func(enum hlsl_intrinsic_functions func);

void hlsl_define_const_reg(struct hlsl_parser *hlsl_ctx, struct _D3DXCONSTANT_DESC *var);
void hlslparser_message(struct hlsl_parser *ctx, const char *fmt, ...);
void hlsl_set_parse_status(struct hlsl_parser *ctx, enum parse_status status);
void hlsl_compile(struct hlsl_parser *hlsl_ctx);
void hlsl_compile_instruction(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_scope *scope, struct shader_reg *return_reg, struct hlsl_instruction *instr);
BOOL hlsl_compile_expression(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_scope *scope, struct hlsl_expression *expr, struct shader_reg *reg);
void hlsl_add_instruction(struct hlsl_parser *hlsl_ctx, enum hlsl_instruction_type, struct hlsl_parameter* param);
struct hlsl_function *hlsl_find_function(struct hlsl_parser *hlsl_ctx, const char *name);
struct bwriter_shader *parse_hlsl_shader(char **messages, const char *main_func, const char *target);
struct bwriter_shader *CompileShader(const char *text, char **messages, const char *main_func, const char *target);
inline struct hlsl_value_size parse_value_size(char *str);
inline float hlsl_get_value(LPVOID val, enum D3DXPARAMETER_TYPE type, unsigned int x, unsigned int y);

BOOL hlsl_get_custom_type(struct hlsl_scope *scope, const char *name, struct hlsl_data_type **value);
BOOL hlsl_add_custom_type(struct hlsl_scope *scope, const char *name, struct hlsl_data_type value);
BOOL hlsl_get_variable(struct hlsl_scope *scope, char *name, struct hlsl_variable **value);
BOOL hlsl_find_var_in_struct(struct hlsl_scope *scope, const char *name, struct hlsl_variable **value);
void hlsl_declare_input(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_semantic *semantic, struct shader_reg *reg);
void hlsl_declare_output(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_semantic *semantic, struct shader_reg *reg);
/*
void hlsl_declare_constant(struct hlsl_parser *hlsl_ctx, struct hlsl_data_type *value, struct shader_reg *reg);
void hlsl_copy_variable(struct hlsl_data_type *in, struct hlsl_data_type *out);
void hlsl_convert_values(struct hlsl_parameter *in, struct hlsl_data_type *out);*/
void hlsl_push_scope(struct hlsl_scope **old, const char *target);
void hlsl_pop_scope(struct hlsl_scope **old);
struct bwriter_shader *merge_bwriter_shaders(struct bwriter_shader *first, struct bwriter_shader *second);

/*inline BOOL hlsl_get_bool(struct hlsl_data_type *in, int r, int c);
inline double hlsl_get_double(struct hlsl_data_type *in, int r, int c);
inline float hlsl_get_float(struct hlsl_data_type *in, int r, int c);
inline int hlsl_get_int(struct hlsl_data_type *in, int r, int c);
inline char * hlsl_get_string(struct hlsl_data_type *in, int r, int c);
inline unsigned int hlsl_get_uint(struct hlsl_data_type *in, int r, int c);*/

/* Operator helper functions */
/*inline void hlsl_operator_inc(struct hlsl_parser *hlsl_ctx, struct hlsl_data_type *var);
inline void hlsl_operator_dec(struct hlsl_parser *hlsl_ctx, struct hlsl_data_type *var);
inline void hlsl_operator_add(struct hlsl_parser *hlsl_ctx, struct hlsl_data_type *var, struct hlsl_data_type *add);
inline void hlsl_operator_sub(struct hlsl_data_type *var, struct hlsl_data_type *sub);
inline void hlsl_operator_mul(struct hlsl_data_type *var, struct hlsl_data_type *mul);
inline void hlsl_operator_div(struct hlsl_data_type *var, struct hlsl_data_type *div);
inline void hlsl_operator_mod(struct hlsl_data_type *var, struct hlsl_data_type *mod);
inline void hlsl_operator_shl(struct hlsl_data_type *var, struct hlsl_data_type *shl);
inline void hlsl_operator_shr(struct hlsl_data_type *var, struct hlsl_data_type *shr);
inline void hlsl_operator_and(struct hlsl_data_type *var, struct hlsl_data_type *and);
inline void hlsl_operator_or(struct hlsl_data_type *var, struct hlsl_data_type *or);
inline void hlsl_operator_xor(struct hlsl_data_type *var, struct hlsl_data_type *xor);*/
