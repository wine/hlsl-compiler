/*
 * Copyright (C) 2010 Matteo Bruni
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */
#define COBJMACROS
#include "wine/test.h"

#include <d3d9types.h>
#include <d3dcommon.h>
#include <d3dcompiler.h>

struct shader_test {
    const char *text;
    const DWORD bytes[128];
};

static void dump_shader(DWORD *shader) {
    unsigned int i = 0; //, j = 0;
    do {
        trace("0x%08x ", shader[i]);
        i++;
        //if(j == 6) trace("\n");
    } while(shader[i - 1] != D3DSIO_END);
    trace("\n");
}

static void exec_tests(const char *name, struct shader_test tests[], unsigned int count) {
    HRESULT hr;
    DWORD *res;
    unsigned int i, j;
    BOOL diff;
    LPD3DBLOB shader, messages;

    for(i = 0; i < count; i++) {
        /* D3DCompile sets messages to 0 if there aren't error messages */
        messages = NULL;
        hr = D3DCompile(tests[i].text, strlen(tests[i].text), NULL,
                        NULL, NULL, "main", "vs_2_0",
                        D3DCOMPILE_SKIP_VALIDATION, 0,
                        &shader, &messages);
        ok(hr == S_OK, "Test %s, shader %d: D3DCompile failed with error 0x%x - %d\n", name, i, hr, hr & 0x0000FFFF);
        if(messages) {
            trace("D3DCompile messages:\n%s", (char *)ID3D10Blob_GetBufferPointer(messages));
            ID3D10Blob_Release(messages);
        }
        if(FAILED(hr)) continue;

        j = 0;
        diff = FALSE;
        res = ID3D10Blob_GetBufferPointer(shader);
        while(res[j] != D3DSIO_END && tests[i].bytes[j] != D3DSIO_END) {
            if(res[j] != tests[i].bytes[j]) diff = TRUE;
            j++;
        };
        /* Both must have an end token */
        if(res[j] != tests[i].bytes[j]) diff = TRUE;

        if(diff) {
            ok(FALSE, "Test %s, shader %d: Generated code differs\n", name, i);
            dump_shader(res);
        }
        ID3D10Blob_Release(shader);
    }
}

static void preproc_test(void) {
    struct shader_test tests[] = {

    };

    exec_tests("preproc", tests, sizeof(tests) / sizeof(tests[0]));
}

static void vs_2_0_test(void) {
    struct shader_test tests[] = {
        {   /* shader 0 */
            "float4 light;\r\n"
            "float4x4 mat;\r\n"
            "\r\n"
            "struct input{\r\n"
            "    float4 position : POSITION;\r\n"
            "    float3 normal : NORMAL;\r\n"
            "};\r\n"
            "\r\n"
            "struct output{\r\n"
            "    float4 position : POSITION;\r\n"
            "    float4 diffuse : COLOR;\r\n"
            "};\r\n"
            "\r\n"
            "output main(const input v){\r\n"
            "    output o;\r\n"
            "\r\n"
            "    o.position = mul(v.position, mat);\r\n"
            "    o.diffuse = dot(light, v.normal);\r\n"
            "\r\n"
            "    return o;\r\n"
            "}\r\n",
	    {0xfffe0200, 0x002bfffe, 0x42415443, 0x0000001c, 0x00000077,
	     0xfffe0200, 0x00000002, 0x0000001c, 0x00000102, 0x00000070,
	     0x00000044, 0x00040002, 0x00000001, 0x0000004c, 0x00000000,
	     0x0000005c, 0x00000002, 0x00000004, 0x00000060, 0x00000000,
	     0x6867696c, 0xabab0074, 0x00030001, 0x00040001, 0x00000001,
	     0x00000000, 0x0074616d, 0x00030003, 0x00040004, 0x00000001,
	     0x00000000, 0x325f7376, 0x4d00305f, 0x6f726369, 0x74666f73,
	     0x29522820, 0x534c4820, 0x6853204c, 0x72656461, 0x6d6f4320,
	     0x656c6970, 0x2e392072, 0x392e3932, 0x332e3235, 0x00313131,
	     0x0200001f, 0x80000000, 0x900f0000, 0x0200001f, 0x80000003,
	     0x900f0001, 0x03000009, 0xc0010000, 0x90e40000, 0xa0e40000,
	     0x03000009, 0xc0020000, 0x90e40000, 0xa0e40001, 0x03000009,
	     0xc0040000, 0x90e40000, 0xa0e40002, 0x03000009, 0xc0080000,
	     0x90e40000, 0xa0e40003, 0x03000008, 0xd00f0000, 0xa0e40004,
	     0x90e40001, 0x0000ffff}
        },
    };

    exec_tests("vs_2_0", tests, sizeof(tests) / sizeof(tests[0]));
}

/* static void failure_test(void) { */
/*     const char * tests[] = { */
/*         /\* shader 0: instruction modifier not allowed *\/ */
/*         "ps_3_0\n" */
/*         "dcl_2d s2\n" */
/*         "texldd_x2 r0, v1, s2, v3, v4\n", */
/*         /\* shader 1: coissue not supported in vertex shaders *\/ */
/*         "vs.1.1\r\n" */
/*         "add r0.rgb, r0, r1\n" */
/*         "+add r0.a, r0, r2\n", */
/*         /\* shader 2: coissue not supported in pixel shader version >= 2.0 *\/ */
/*         "ps_2_0\n" */
/*         "texld r0, t0, s0\n" */
/*         "add r0.rgb, r0, r1\n" */
/*         "+add r0.a, r0, v1\n", */
/*         /\* shader 3: predicates not supported in vertex shader < 2.0 *\/ */
/*         "vs_1_1\n" */
/*         "(p0) add r0, r0, v0\n", */
/*         /\* shader 4: register a0 doesn't exist in pixel shaders *\/ */
/*         "ps_3_0\n" */
/*         "mov r0, v[ a0 + 12 ]\n", */
/*         /\* shader 5: s0 doesn't exist in vs_1_1 *\/ */
/*         "vs_1_1\n" */
/*         "mov r0, s0\n", */
/*         /\* shader 6: aL is a scalar register, no swizzles allowed *\/ */
/*         "ps_3_0\n" */
/*         "mov r0, v[ aL.x + 12 ]\n", */
/*         /\* shader 7: tn doesn't exist in ps_3_0 *\/ */
/*         "ps_3_0\n" */
/*         "dcl_2d s2\n" */
/*         "texldd r0, t1, s2, v3, v4\n", */
/*         /\* shader 8: two shift modifiers *\/ */
/*         "ps_1_3\n" */
/*         "mov_x2_x2 r0, r1\n", */
/*         /\* shader 9: too many source registers for mov instruction *\/ */
/*         "vs_1_1\n" */
/*         "mov r0, r1, r2\n", */
/*         /\* shader 10: invalid combination of negate and divide modifiers *\/ */
/*         "ps_1_4\n" */
/*         "texld r5, -r2_dz\n", */
/*         /\* shader 11: complement modifier not allowed in >= PS 2 *\/ */
/*         "ps_2_0\n" */
/*         "mov r2, 1 - r0\n", */
/*         /\* shader 12: invalid modifier *\/ */
/*         "vs_3_0\n" */
/*         "mov r2, 2 - r0\n", */
/*         /\* shader 13: float value in relative addressing *\/ */
/*         "vs_3_0\n" */
/*         "mov r2, c[ aL + 3.4 ]\n", */
/*         /\* shader 14: complement modifier not available in VS *\/ */
/*         "vs_3_0\n" */
/*         "mov r2, 1 - r1\n", */
/*         /\* shader 15: _x2 modifier not available in VS *\/ */
/*         "vs_1_1\n" */
/*         "mov r2, r1_x2\n", */
/*         /\* shader 16: _abs modifier not available in < VS 3.0 *\/ */
/*         "vs_1_1\n" */
/*         "mov r2, r1_abs\n", */
/*         /\* shader 17: _x2 modifier not available in >= PS 2.0 *\/ */
/*         "ps_2_0\n" */
/*         "mov r0, r1_x2\n", */
/*         /\* shader 18: wrong swizzle *\/ */
/*         "vs_2_0\n" */
/*         "mov r0, r1.abcd\n", */
/*         /\* shader 19: wrong swizzle *\/ */
/*         "vs_2_0\n" */
/*         "mov r0, r1.xyzwx\n", */
/*         /\* shader 20: wrong swizzle *\/ */
/*         "vs_2_0\n" */
/*         "mov r0, r1.\n", */
/*         /\* shader 21: invalid writemask *\/ */
/*         "vs_2_0\n" */
/*         "mov r0.xxyz, r1\n", */
/*         /\* shader 22: register r5 doesn't exist in PS < 1.4 *\/ */
/*         "ps_1_3\n" */
/*         "mov r5, r0\n", */
/*         /\* shader 23: can't declare output registers in a pixel shader *\/ */
/*         "ps_3_0\n" */
/*         "dcl_positiont o0\n", */
/*         /\* shader 24: _pp instruction modifier not allowed in vertex shaders *\/ */
/*         "vs_3_0\n" */
/*         "add_pp r0, r0, r1\n", */
/*         /\* shader 25: _x4 instruction modified not allowed in > ps_1_x *\/ */
/*         "ps_3_0\n" */
/*         "add_x4 r0, r0, r1\n", */
/*         /\* shader 26: there aren't oCx registers in ps_1_x *\/ */
/*         "ps_1_3\n" */
/*         "add oC0, r0, r1\n", */
/*         /\* shader 27: oC3 is the max in >= ps_2_0 *\/ */
/*         "ps_3_0\n" */
/*         "add oC4, r0, r1\n", */
/*         /\* shader 28: register v17 doesn't exist *\/ */
/*         "vs_3_0\n" */
/*         "add r0, r0, v17\n", */
/*         /\* shader 29: register o13 doesn't exist *\/ */
/*         "vs_3_0\n" */
/*         "add o13, r0, r1\n", */
/*         /\* shader 30: label > 2047 not allowed *\/ */
/*         "vs_3_0\n" */
/*         "call l2048\n", */
/*         /\* shader 31: s20 register does not exist *\/ */
/*         "ps_3_0\n" */
/*         "texld r0, r1, s20\n", */
/*         /\* shader 32: t5 not allowed in ps_1_3 *\/ */
/*         "ps_1_3\n" */
/*         "tex t5\n", */
/*         /\* shader 33: no temporary registers relative addressing *\/ */
/*         "vs_3_0\n" */
/*         "add r0, r0[ a0.x ], r1\n", */
/*         /\* shader 34: no input registers relative addressing in vs_2_0 *\/ */
/*         "vs_2_0\n" */
/*         "add r0, v[ a0.x ], r1\n", */
/*         /\* shader 35: no aL register in ps_2_0 *\/ */
/*         "ps_2_0\n" */
/*         "add r0, v[ aL ], r1\n", */
/*         /\* shader 36: no relative addressing in ps_2_0 *\/ */
/*         "ps_2_0\n" */
/*         "add r0, v[ r0 ], r1\n", */
/*         /\* shader 37: no a0 register in ps_3_0 *\/ */
/*         "ps_3_0\n" */
/*         "add r0, v[ a0.x ], r1\n", */
/*         /\* shader 38: only a0.x accepted in vs_1_1 *\/ */
/*         "vs_1_1\n" */
/*         "mov r0, c0[ a0 ]\n", */
/*         /\* shader 39: invalid modifier for dcl instruction *\/ */
/*         "ps_3_0\n" */
/*         "dcl_texcoord0_sat v0\n", */
/*         /\* shader 40: shift not allowed *\/ */
/*         "ps_3_0\n" */
/*         "dcl_texcoord0_x2 v0\n", */
/*         /\* shader 41: no modifier allowed with dcl instruction in vs *\/ */
/*         "vs_3_0\n" */
/*         "dcl_texcoord0_centroid v0\n", */
/*         /\* shader 42: no modifiers with vs dcl sampler instruction *\/ */
/*         "vs_3_0\n" */
/*         "dcl_2d_pp s0\n", */
/*         /\* shader 43: *\/ */
/*         "ps_1_3\n" */
/*         "texm3x3vspec t3, t0\n", */
/*         /\* shader 44: *\/ */
/*         "ps_2_0\n" */
/*         "texm3x3vspec t3, t0\n", */
/*     }; */
/*     HRESULT hr; */
/*     unsigned int i; */
/*     LPD3DBLOB shader, messages; */

/*     for(i = 0; i < (sizeof(tests) / sizeof(tests[0])); i++) { */
/*         shader = NULL; */
/*         messages = NULL; */
/*         hr = D3DAssemble(tests[i], strlen(tests[i]), */
/* 			 NULL, NULL, D3DCOMPILE_SKIP_VALIDATION, */
/* 			 &shader, &messages); */
/* 	ok(hr == E_INVALIDARG, "Failure test, shader %d: " */
/*            "expected D3DAssemble failure with E_INVALIDARG, " */
/*            "got 0x%x - %d\n", i, hr, hr & 0x0000FFFF); */
/* 	if(messages) { */
/*             trace("D3DAssemble messages:\n%s", (char *)ID3D10Blob_GetBufferPointer(messages)); */
/*             ID3D10Blob_Release(messages); */
/*         } */
/*         if(shader) { */
/*             DWORD *res = ID3D10Blob_GetBufferPointer(shader); */
/*             dump_shader(res); */
/*             ID3D10Blob_Release(shader); */
/* 	} */
/*     } */
/* } */

/*
static HRESULT WINAPI testD3DXInclude_open(ID3DXInclude *iface,
                                           D3DXINCLUDE_TYPE include_type,
                                           LPCSTR filename, LPCVOID parent_data,
                                           LPCVOID *data, UINT *bytes) {
    char *buffer;
    char include[] = "#define REGISTER r0\nvs.1.1\n";

    trace("filename = %s\n",filename);

    buffer = HeapAlloc(GetProcessHeap(), 0, sizeof(include));
    CopyMemory(buffer, include, sizeof(include));
    *data = buffer;
    *bytes = sizeof(include);
    return S_OK;
}

static HRESULT WINAPI testD3DXInclude_close(ID3DXInclude *iface, LPCVOID data) {
    HeapFree(GetProcessHeap(), 0, (LPVOID)data);
    return S_OK;
}

static const struct ID3DXIncludeVtbl D3DXInclude_Vtbl = {
    testD3DXInclude_open,
    testD3DXInclude_close
};

struct D3DXIncludeImpl {
    const ID3DXIncludeVtbl *lpVtbl;
};
*/
/* static void assembleshader_test(void) { */
/*     const char test1[] = { */
/*         "vs.1.1\n" */
/*         "mov DEF2, v0\n" */
/*     }; */
/*     const char testincl[] = { */
/*         "#define REGISTER r0\n" */
/*         "vs.1.1\n" */
/*     }; */
/*     const char testshader[] = { */
/*         "#include \"incl.vsh\"\n" */
/*         "mov REGISTER, v0\n" */
/*     }; */
/*     HRESULT hr; */
/*     LPD3DXBUFFER shader, messages; */
/*     D3DXMACRO defines[] = { */
/*         { */
/*             "DEF1", "10 + 15" */
/*         }, */
/*         { */
/*             "DEF2", "r0" */
/*         }, */
/*         { */
/*             NULL, NULL */
/*         } */
/*     }; */
/*     struct D3DXIncludeImpl include; */
/*     HRESULT shader_vsh_res, incl_vsh_res; */

/*     /\* pDefines test *\/ */
/*     shader = NULL; */
/*     messages = NULL; */
/*     hr = D3DXAssembleShader(test1, strlen(test1), */
/*                             defines, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                             &shader, &messages); */
/*     ok(hr == D3D_OK, "pDefines test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     /\* NULL messages test *\/ */
/*     shader = NULL; */
/*     hr = D3DXAssembleShader(test1, strlen(test1), */
/*                             defines, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                             &shader, NULL); */
/*     ok(hr == D3D_OK, "NULL messages test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     /\* NULL shader test *\/ */
/*     messages = NULL; */
/*     hr = D3DXAssembleShader(test1, strlen(test1), */
/*                             defines, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                             NULL, &messages); */
/*     ok(hr == D3D_OK, "NULL shader test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */

/*     /\* pInclude test *\/ */
/*     shader = NULL; */
/*     messages = NULL; */
/*     include.lpVtbl = &D3DXInclude_Vtbl; */
/*     hr = D3DXAssembleShader(testshader, strlen(testshader), */
/*                             NULL, (LPD3DXINCLUDE)&include, D3DXSHADER_SKIPVALIDATION, */
/*                             &shader, &messages); */
/*     ok(hr == D3D_OK, "pInclude test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     shader_vsh_res = create_file("shader.vsh", testshader, sizeof(testshader)); */
/*     if(SUCCEEDED(shader_vsh_res)) { */
/*         incl_vsh_res = create_file("incl.vsh", testincl, sizeof(testincl)); */
/*         if(SUCCEEDED(incl_vsh_res)) { */
/*             /\* D3DXAssembleShaderFromFile + #include test *\/ */
/*             shader = NULL; */
/*             messages = NULL; */
/*             hr = D3DXAssembleShaderFromFileA("shader.vsh", */
/*                                              NULL, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                                              &shader, &messages); */
/*             ok(hr == D3D_OK, "D3DXAssembleShaderFromFile test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*             if(messages) { */
/*                 trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*                 ID3DXBuffer_Release(messages); */
/*             } */
/*             if(shader) ID3DXBuffer_Release(shader); */
/*         } else skip("Couldn't create \"incl.vsh\"\n"); */

/*         /\* D3DXAssembleShaderFromFile + pInclude test *\/ */
/*         shader = NULL; */
/*         messages = NULL; */
/*         hr = D3DXAssembleShaderFromFileA("shader.vsh", */
/*                                          NULL, (LPD3DXINCLUDE)&include, D3DXSHADER_SKIPVALIDATION, */
/*                                          &shader, &messages); */
/*         ok(hr == D3D_OK, "D3DXAssembleShaderFromFile + pInclude test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*         if(messages) { */
/*             trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*             ID3DXBuffer_Release(messages); */
/*         } */
/*         if(shader) ID3DXBuffer_Release(shader); */
/*     } else skip("Couldn't create \"shader.vsh\"\n"); */

/*     /\* NULL shader tests *\/ */
/*     shader = NULL; */
/*     messages = NULL; */
/*     hr = D3DXAssembleShader(NULL, 0, */
/*                             NULL, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                             &shader, &messages); */
/*     ok(hr == D3DXERR_INVALIDDATA, "NULL shader test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShader messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     shader = NULL; */
/*     messages = NULL; */
/*     hr = D3DXAssembleShaderFromFileA("nonexistent.vsh", */
/*                                      NULL, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                                      &shader, &messages); */
/*     ok(hr == D3DXERR_INVALIDDATA || hr == E_FAIL, /\* I get this on WinXP *\/ */
/*         "D3DXAssembleShaderFromFile nonexistent file test failed with error 0x%x - %d\n", */
/*         hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShaderFromFile messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     /\* D3DXAssembleShaderFromResource test *\/ */
/*     shader = NULL; */
/*     messages = NULL; */
/*     hr = D3DXAssembleShaderFromResourceA(NULL, MAKEINTRESOURCEA(IDB_ASMSHADER), */
/*                                          NULL, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                                          &shader, &messages); */
/*     ok(hr == D3D_OK, "D3DXAssembleShaderFromResource test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShaderFromResource messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     /\* D3DXAssembleShaderFromResource with missing shader resource test *\/ */
/*     shader = NULL; */
/*     messages = NULL; */
/*     hr = D3DXAssembleShaderFromResourceA(NULL, "notexisting", */
/*                                          NULL, NULL, D3DXSHADER_SKIPVALIDATION, */
/*                                          &shader, &messages); */
/*     ok(hr == D3DXERR_INVALIDDATA, "D3DXAssembleShaderFromResource NULL shader test failed with error 0x%x - %d\n", hr, hr & 0x0000FFFF); */
/*     if(messages) { */
/*         trace("D3DXAssembleShaderFromResource messages:\n%s", (char *)ID3DXBuffer_GetBufferPointer(messages)); */
/*         ID3DXBuffer_Release(messages); */
/*     } */
/*     if(shader) ID3DXBuffer_Release(shader); */

/*     /\* cleanup *\/ */
/*     if(SUCCEEDED(shader_vsh_res)) { */
/*         DeleteFileA("shader.vsh"); */
/*         if(SUCCEEDED(incl_vsh_res)) DeleteFileA("incl.vsh"); */
/*     } */
/* } */

START_TEST(hlsl)
{
    preproc_test();
    vs_2_0_test();
}
