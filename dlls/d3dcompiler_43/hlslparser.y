/*
 * Direct3D HLSL compiler
 *
 * Copyright 2010 Matijn Woudt
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%{
#include "config.h"
#include "wine/port.h"
#include "wine/debug.h"

#include "hlsl.h"
#include <stdio.h>
#include "winbase.h"

WINE_DEFAULT_DEBUG_CHANNEL(d3dcompiler);
WINE_DECLARE_DEBUG_CHANNEL(hlsl_parsed);

/* Needed lexer functions declarations */
void hlslparser_error(const char *s);
int hlslparser_lex(void);

%}

%union {
    unsigned int integer;
    float floating;
    enum hlsl_data_type_kind type;
    struct hlsl_data_type *data_type;
    struct hlsl_variable *variable;
    struct hlsl_struct *structure;
    struct hlsl_parameter *parameter;
    struct hlsl_var_defs {
        unsigned int count;
        struct var_type_size *vars_type_size;
    } var_defs;
    struct hlsl_semantic *semantic;
    char *str;
    float immval;
    struct hlsl_expression *expr;
    struct shader_reg *reg;
    enum hlsl_operator operator;
    enum hlsl_intrinsic_functions func;
}

%type <type> scalar
%type <data_type> var_data_type
%type <variable> var_func_spec
%type <variable> variable_decl
%type <semantic> semantic
%type <structure> struct_member_list
%type <parameter> return_value
%type <expr> initial_value
%type <expr> expression
%type <expr> expression_a
%type <expr> expression_b
%type <expr> expression_c
%type <expr> expression_d
%type <expr> expression_e
%type <expr> expression_f
%type <expr> expression_g
%type <expr> expression_h
%type <expr> expression_i
%type <expr> expression_j
%type <expr> expression_k
%type <expr> expression_l
%type <expr> expression_m
%type <parameter> parameters
%type <expr> function
%type <operator> assign_operator

%token <integer> INTEGER
%token <floating> FLOATING
%token <str> IDENTIFIER
%token <data_type> CUSTOM_TYPE
%token <variable> VARIABLE
%token <func> FUNCTION
%token BOOLEAN_AND
%token BOOLEAN_OR
%token SHIFT_LEFT
%token SHIFT_RIGHT
%token VAR_INC
%token VAR_DEC
%token ASSIGN
%token ASSIGN_ADD
%token ASSIGN_SUB
%token ASSIGN_MUL
%token ASSIGN_DIV
%token ASSIGN_MOD
%token ASSIGN_SHIFT_LEFT
%token ASSIGN_SHIFT_RIGHT
%token ASSIGN_BITW_AND
%token ASSIGN_BITW_XOR
%token ASSIGN_BITW_OR
%token EQUALS
%token NOT_EQUALS
%token LESS_EQUALS
%token GREATER_EQUALS

%token KW_ALLOW_UAV_CONDITION
%token <integer> KW_BINORMAL
%token <integer> KW_BLENDINDICES
%token KW_BLENDSTATE
%token <integer> KW_BLENDWEIGHT
%token KW_BOOL
%token KW_BRANCH
%token KW_BREAK
%token KW_BUFFER
%token KW_CALL
%token KW_CASE
%token KW_CBUFFER
%token KW_CENTROID
%token KW_CLASS
%token <integer> KW_COLOR
%token KW_COLUMN_MAJOR
%token KW_COMPILE
%token KW_CONST
%token KW_CONTINUE
%token KW_DEFAULT
%token <integer> KW_DEPTH
%token KW_DEPTHSTENCILSTATE
%token KW_DEPTHSTENCILVIEW
%token KW_DISCARD
%token KW_DO
%token KW_DOUBLE
%token KW_ELSE
%token KW_EXTERN
%token KW_FALSE
%token KW_FASTOPT
%token KW_FLATTEN
%token KW_FLOAT
%token KW_FOG
%token KW_FOR
%token KW_FORCECASE
%token KW_GEOMETRYSHADER
%token KW_GROUPSHARED
%token KW_HALF
%token KW_IF
%token KW_IN
%token KW_INLINE
%token KW_INOUT
%token KW_INT
%token KW_INTERFACE
%token KW_LINEAR
%token KW_LOOP
%token KW_MATRIX
%token KW_NAMESPACE
%token KW_NOINTERPOLATION
%token KW_NOPERSPECTIVE
%token <integer> KW_NORMAL
%token KW_OUT
%token KW_PACKOFFSET
%token KW_PASS
%token KW_PIXELSHADER
%token <integer> KW_POSITION
%token KW_POSITIONT
%token KW_PRECISE
%token <integer> KW_PSIZE
%token KW_RASTERIZERSTATE
%token KW_REGISTER
%token KW_RENDERTARGETVIEW
%token KW_RETURN
%token KW_ROW_MAJOR
%token KW_SAMPLE
%token KW_SAMPLER
%token KW_SAMPLER1D
%token KW_SAMPLER2D
%token KW_SAMPLER3D
%token KW_SAMPLERCUBE
%token KW_SAMPLER_STATE
%token KW_SAMPLERCOMPARISONSTATE
%token KW_SHARED
%token KW_STATEBLOCK
%token KW_STATEBLOCK_STATE
%token KW_STATIC
%token KW_STRING
%token KW_STRUCT
%token KW_SWITCH
%token <integer> KW_TANGENT
%token KW_TBUFFER
%token KW_TECHNIQUE
%token KW_TECHNIQUE10
%token <integer> KW_TESSFACTOR
%token <integer> KW_TEXCOORD
%token KW_TEXTURE
%token KW_TEXTURE1D
%token KW_TEXTURE1DARRAY
%token KW_TEXTURE2D
%token KW_TEXTURE2DARRAY
%token KW_TEXTURE2DMS
%token KW_TEXTURE2DMSARRAY
%token KW_TEXTURE3D
%token KW_TEXTURECUBE
%token KW_TEXTURECUBEARRAY
%token KW_TRUE
%token KW_TYPEDEF
%token KW_UINT
%token KW_UNIFORM
%token KW_UNROLL
%token KW_VECTOR
%token KW_VERTEXSHADER
%token KW_VFACE
%token KW_VOID
%token KW_VOLATILE
%token KW_VPOS
%token KW_WHILE

%%

hlsl_shader:            /* empty */ | hlsl_shader global_decl

global_decl:            function_decl
                        | struct_decl
                        | typedef_decl
                        | variable_decl
                        {
                            /* Declare global variable */
                            hlsl_ctx.current_scope->vars[hlsl_ctx.current_scope->var_count++] = $1;
                        }

struct_decl:            KW_STRUCT IDENTIFIER '{' struct_member_list '}' ';'
                        {
                            /* FIXME: Move to global_decl/local_decl */
                            struct hlsl_data_type data_type;
                            data_type.type = HLSL_STRUCT;
                            data_type.value.structure = *$4;
                            hlsl_add_custom_type(hlsl_ctx.current_scope, $2, data_type);
                        }

struct_member_list:     variable_decl
                        {
                            //TODO: replace with a list?
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_struct));
                            $$->members = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_variable));
                            $$->members[0].name = $1->name;
                            $$->members[0].data_type = $1->data_type;
                            $$->count = 1;
                            //FIXME: free variable_decl
                        }
                        | struct_member_list variable_decl
                        {
                            $$->members = HeapReAlloc(GetProcessHeap(), 0, $$->members, ($$->count + 1) * sizeof(struct hlsl_variable));
                            $$->members[$$->count].name = $2->name;
                            $$->members[$$->count].data_type = $2->data_type;
                            $$->count++;
                            //FIXME: free variable_decl
                        }

typedef_decl:           KW_TYPEDEF opt_const IDENTIFIER var_data_type index
                        {
                            hlsl_add_custom_type(hlsl_ctx.current_scope, $3, *$4);
                        }

opt_const:              /* empty */ | KW_CONST

variable_decl:          var_func_spec index semantic annotations initial_value packoffset register ';'
                        {
                            $$ = $1;
                            if($5) {
                                struct hlsl_instruction *instr;
                                struct hlsl_parameter *param;
                                instr = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_instruction));
                                param = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_parameter));
                                $5->A.var = $1;
                                param->value = $5;
                                param->next = NULL;
                                instr->type = HLSL_INSTR_STATEMENT;
                                instr->args = param;
                                instr->next = NULL;
                                if(hlsl_ctx.current_scope->last_instr == NULL) {
                                    hlsl_ctx.current_scope->first_instr = instr;
                                } else {
                                    hlsl_ctx.current_scope->last_instr->next = instr;
                                }
                                hlsl_ctx.current_scope->last_instr = instr;
                            }
                        }

                        /* Note: storage_class and type_modifier are commented out because
                           they cause 224 shift/reduce errors when adding constructor-like
                           function call to function grammar. */
var_func_spec:          /*storage_class type_modifier*/ var_data_type IDENTIFIER
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_variable));
                            $$->data_type = $1;
                            $$->name = $2;
                        }

/*storage_class:          /* empty */
/*                        | KW_EXTERN
                        | KW_NOINTERPOLATION
                        | KW_PRECISE
                        | KW_SHARED
                        | KW_GROUPSHARED
                        | KW_STATIC
                        | KW_UNIFORM
                        | KW_VOLATILE
*/

type_modifier:          /* empty */
                        | KW_CONST
                        | KW_ROW_MAJOR
                        | KW_COLUMN_MAJOR

var_data_type:          KW_BUFFER '<' scalar '>'
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = $3;
                            $$->rows = -1;
                            $$->cols = -1;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | scalar
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = $1;
                            $$->rows = 1;
                            $$->cols = 1;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | KW_VECTOR '<' scalar ',' INTEGER '>'
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = $3;
                            $$->rows = 1;
                            $$->cols = $5;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | KW_MATRIX '<' scalar ',' INTEGER ',' INTEGER '>'
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = $3;
                            $$->rows = $5;
                            $$->cols = $7;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | KW_SAMPLER
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = HLSL_SCALAR_UNK;
                            $$->rows = -1;
                            $$->cols = -1;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | KW_VERTEXSHADER
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = HLSL_SCALAR_UNK;
                            $$->rows = -1;
                            $$->cols = -1;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | KW_TEXTURE
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->type = HLSL_SCALAR_UNK;
                            $$->rows = -1;
                            $$->cols = -1;
                            $$->value_type = HLSL_VALUE_INVALID;
                        }
                        | CUSTOM_TYPE
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            CopyMemory($$, $1, sizeof(struct hlsl_data_type));
                        }

scalar:                 KW_BOOL     {   $$ = HLSL_SCALAR_BOOL;      }
                        | KW_INT    {   $$ = HLSL_SCALAR_INT;       }
                        | KW_UINT   {   $$ = HLSL_SCALAR_UINT;      }
                        | KW_HALF   {   $$ = HLSL_SCALAR_HALF;      }
                        | KW_FLOAT  {   $$ = HLSL_SCALAR_FLOAT;     }
                        | KW_DOUBLE {   $$ = HLSL_SCALAR_DOUBLE;    }
                        | KW_STRING {   $$ = HLSL_SCALAR_STRING;    }

index:                  /* empty */ | '[' expression ']'

semantic:               /* empty */
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_NONE;
                            $$->num = 0;
                        }
                        | KW_BINORMAL
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_BINORMAL;
                            $$->num = $1;
                        }
                        | KW_BLENDINDICES
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_BLENDINDICES;
                            $$->num = $1;
                        }
                        | KW_BLENDWEIGHT
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_BLENDWEIGHT;
                            $$->num = $1;
                        }
                        | KW_COLOR
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_COLOR;
                            $$->num = $1;
                        }
                        | KW_DEPTH
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_DEPTH;
                            $$->num = $1;
                        }
                        | KW_FOG
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_FOG;
                            $$->num = 0;
                        }
                        | KW_NORMAL
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_NORMAL;
                            $$->num = $1;
                        }
                        | KW_POSITION
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_POSITION;
                            $$->num = $1;
                        }
                        | KW_POSITIONT
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_POSITIONT;
                            $$->num = 0;
                        }
                        | KW_PSIZE
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_PSIZE;
                            $$->num = $1;
                        }
                        | KW_TANGENT
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_TANGENT;
                            $$->num = $1;
                        }
                        | KW_TESSFACTOR
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_TESSFACTOR;
                            $$->num = $1;
                        }
                        | KW_TEXCOORD
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_TEXCOORD;
                            $$->num = $1;
                        }
                        | KW_VFACE
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_VFACE;
                            $$->num = 0;
                        }
                        | KW_VPOS
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_semantic));
                            $$->semantic = HLSL_SEM_VPOS;
                            $$->num = 0;
                        }

annotations:            | '<' annotation_list '>'

annotation_list:        | annotation_list annotation

annotation:             var_data_type initial_value ';'

initial_value:          /* empty */
                        {
                            $$ = NULL;
                        }
                        | ASSIGN expression
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_ASSIGN;
                            $$->A.var = NULL;
                            $$->B.expr = $2;
                        }
                        | ASSIGN '{' parameters '}'
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_ASSIGN_PARAM;
                            $$->A.var = NULL;
                            $$->B.params = $3;
                        }

assign_operator:        ASSIGN
                        {
                            $$ = HLSL_OP_ASSIGN;
                        }
                        | ASSIGN_ADD
                        {
                            $$ = HLSL_OP_ASSIGN_ADD;
                        }
                        | ASSIGN_SUB
                        {
                            $$ = HLSL_OP_ASSIGN_SUB;
                        }
                        | ASSIGN_MUL
                        {
                            $$ = HLSL_OP_ASSIGN_MUL;
                        }
                        | ASSIGN_DIV
                        {
                            $$ = HLSL_OP_ASSIGN_DIV;
                        }
                        | ASSIGN_MOD
                        {
                            $$ = HLSL_OP_ASSIGN_MOD;
                        }
                        | ASSIGN_SHIFT_LEFT
                        {
                            $$ = HLSL_OP_ASSIGN_SHIFT_LEFT;
                        }
                        | ASSIGN_SHIFT_RIGHT
                        {
                            $$ = HLSL_OP_ASSIGN_SHIFT_RIGHT;
                        }
                        | ASSIGN_BITW_AND
                        {
                            $$ = HLSL_OP_ASSIGN_BITW_AND;
                        }
                        | ASSIGN_BITW_XOR
                        {
                            $$ = HLSL_OP_ASSIGN_BITW_XOR;
                        }
                        | ASSIGN_BITW_OR
                        {
                            $$ = HLSL_OP_ASSIGN_BITW_OR;
                        }

expression:             expression_a
                        {
                            $$ = $1;
                        }
                        | VARIABLE index assign_operator expression
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = $3;
                            $$->A.var = $1;
                            $$->B.expr = $4;
                        }

expression_a:           expression_b
                        {
                            $$ = $1;
                        }
                        | expression_b '?' expression_a ':' expression_a
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_CONDITIONAL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                            $$->C = $5;
                        }

expression_b:           expression_c
                        {
                            $$ = $1;
                        }
                        | expression_c BOOLEAN_OR expression_b
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_OR;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_c:           expression_d
                        {
                            $$ = $1;
                        }
                        | expression_d BOOLEAN_AND expression_c
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_AND;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_d:           expression_e
                        {
                            $$ = $1;
                        }
                        | expression_e '|' expression_d
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_BITW_OR;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_e:           expression_f
                        {
                            $$ = $1;
                        }
                        | expression_f '^' expression_e
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_BITW_XOR;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_f:           expression_g
                        {
                            $$ = $1;
                        }
                        | expression_g '&' expression_f
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_BITW_AND;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_g:           expression_h
                        {
                            $$ = $1;
                        }
                        | expression_h EQUALS expression_g
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_EQUAL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_h NOT_EQUALS expression_g
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_NOT_EQUAL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_h:           expression_i
                        {
                            $$ = $1;
                        }/* TODO: ... */
                        | expression_i '<' expression_h
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_LESS;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_i LESS_EQUALS expression_h
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_LESS_EQUAL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_i '>' expression_h
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_GREATER;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_i GREATER_EQUALS expression_h
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_GREATER_EQUAL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_i:           expression_j
                        {
                            $$ = $1;
                        }
                        | expression_j SHIFT_LEFT expression_i
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_SHIFT_LEFT;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_j SHIFT_RIGHT expression_i
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_SHIFT_RIGHT;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_j:           expression_k
                        {
                            $$ = $1;
                        }
                        | expression_k '+' expression_j
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_ADD;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_k '-' expression_j
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_SUB;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_k:           expression_l
                        {
                            $$ = $1;
                        }
                        | expression_l '*' expression_k
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_MUL;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_l '/' expression_k
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_DIV;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }
                        | expression_l '%' expression_k
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_MOD;
                            $$->A.expr = $1;
                            $$->B.expr = $3;
                        }

expression_l:           expression_m
                        {
                            $$ = $1;
                        }
                        | '+' expression_m
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_POSITIVE;
                            $$->A.expr = $2;
                        }
                        | '-' expression_m
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_NEGATIVE;
                            $$->A.expr = $2;
                        }
                        | '!' expression_m
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_UNARY_NOT;
                            $$->A.expr = $2;
                        }
                        | '~' expression_m
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_LOGICAL_NOT;
                            $$->A.expr = $2;
                        }
                        | '(' var_data_type ')' expression_l
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_CAST;
                            $$->A.data_type = $2;
                            $$->B.expr = $4;
                        }

expression_m:           INTEGER
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_CONST;
                            $$->A.data_type = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->A.data_type->type = HLSL_SCALAR_UINT;
                            $$->A.data_type->rows = 1;
                            $$->A.data_type->cols = 1;
                            $$->A.data_type->value_type = HLSL_VALUE_IMMEDIATE;
                            $$->A.data_type->value.u = HeapAlloc(GetProcessHeap(), 0, sizeof(unsigned int *));
                            $$->A.data_type->value.u[0] = HeapAlloc(GetProcessHeap(), 0, sizeof(unsigned int));
                            $$->A.data_type->value.u[0][0] = $1;
                        }
                        | FLOATING
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_CONST;
                            $$->A.data_type = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                            $$->A.data_type->type = HLSL_SCALAR_FLOAT;
                            $$->A.data_type->rows = 1;
                            $$->A.data_type->cols = 1;
                            $$->A.data_type->value_type = HLSL_VALUE_IMMEDIATE;
                            $$->A.data_type->value.f = HeapAlloc(GetProcessHeap(), 0, sizeof(float *));
                            $$->A.data_type->value.f[0] = HeapAlloc(GetProcessHeap(), 0, sizeof(float));
                            $$->A.data_type->value.f[0][0] = $1;
                        }
                        | function
                        {
                            FIXME("expression: function\n");
                            $$ = $1;
                        }
                        | VARIABLE index
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_VAR;
                            $$->A.var = $1;
                        }
                        | VARIABLE index VAR_INC
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_VAR_INC;
                            $$->A.var = $1;
                        }
                        | VARIABLE index VAR_DEC
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_VAR_DEC;
                            $$->A.var = $1;
                        }
                        | VAR_INC VARIABLE index
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_PRE_INC;
                            $$->A.var = $2;
                        }
                        | VAR_DEC VARIABLE index
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_PRE_DEC;
                            $$->A.var = $2;
                        }
                        | '(' expression ')'
                        {
                            FIXME("expression: '(' expression ')' \n");
                            $$ = $2;
                        }

function:               FUNCTION '(' parameters ')'
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_expression));
                            $$->op = HLSL_OP_INTRINSIC_FUNC;
                            $$->A.func = $1;
                            $$->B.params = $3;
                        }
                        | IDENTIFIER '(' parameters ')'
                        {
                            FIXME("Function call\n");
                            $$ = NULL;
                        }
                        | var_data_type '(' parameters ')'
                        {
                            FIXME("Constructor-like function call\n");
                            $$ = NULL;
                        }

parameters:             expression
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_parameter));
                            $$->next = NULL;
                            $$->value = $1;
                        }
                        | expression ',' parameters
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_parameter));
                            $$->next = $3;
                            $$->value = $1;
                        }

packoffset:             /* empty */ | KW_PACKOFFSET '(' IDENTIFIER ')'

register:               /* empty */ | KW_REGISTER '(' IDENTIFIER ')'

function_decl:          function_head ';'
                        | function_head '{' statement_block '}'
                        {
                            hlsl_ctx.functions[hlsl_ctx.function_count++]->scope = hlsl_ctx.current_scope;
                            hlsl_pop_scope(&hlsl_ctx.current_scope);
                        }

function_head:          var_func_spec
                        {
                            hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target);
                        }
                        '(' argument_list ')' semantic
                        {
                            struct hlsl_function *func;
                            func = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(struct hlsl_function));
                            func->name = $1->name;
                            func->return_type = $1->data_type;
                            if($6->semantic != HLSL_SEM_NONE) {
                                func->return_type->value_type = HLSL_VALUE_SEMANTIC;
                                func->return_type->value.sem = $6;
                            }
                            hlsl_ctx.functions[hlsl_ctx.function_count] = func;
                        }

argument_list:          /* empty */ | argument | argument_list ',' argument /* FIXME: ',' at end shouldn't be possible */

argument:               input_modifier type_modifier var_data_type IDENTIFIER semantic interpolation_modifier initial_value
                        {
                            struct hlsl_variable *var = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_variable));
                            var->data_type = $3;
                            var->name = $4;
                            if($5->semantic != HLSL_SEM_NONE) {
                                var->data_type->value_type = HLSL_VALUE_SEMANTIC;
                                var->data_type->value.sem = $5;
                            }
                            hlsl_ctx.current_scope->vars[hlsl_ctx.current_scope->var_count++] = var;
                        }

input_modifier:         /* empty */
                        | KW_IN
                        | KW_INOUT
                        | KW_OUT
                        | KW_UNIFORM

interpolation_modifier: /* empty */
                        | KW_CENTROID
                        | KW_LINEAR
                        | KW_NOINTERPOLATION
                        | KW_NOPERSPECTIVE
                        | KW_SAMPLE

statement_block:        statement | statement_block statement

statement:              KW_RETURN return_value ';'
                        {
                            struct hlsl_instruction *instr;
                            instr = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_instruction));
                            instr->type = HLSL_INSTR_RETURN;
                            instr->args = $2;
                            instr->next = NULL;
                            if(hlsl_ctx.current_scope->last_instr == NULL) {
                                hlsl_ctx.current_scope->first_instr = instr;
                            } else {
                                hlsl_ctx.current_scope->last_instr->next = instr;
                            }
                            hlsl_ctx.current_scope->last_instr = instr;
                        }
                        | variable_decl
                        {
                            hlsl_ctx.current_scope->vars[hlsl_ctx.current_scope->var_count++] = $1;
                        }
                        | expression ';'
                        {
                            struct hlsl_instruction *instr;
                            struct hlsl_parameter *param;
                            instr = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_instruction));
                            param = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_parameter));
                            param->value = $1;
                            param->next = NULL;
                            instr->type = HLSL_INSTR_STATEMENT;
                            instr->args = param;
                            instr->next = NULL;
                            if(hlsl_ctx.current_scope->last_instr == NULL) {
                                hlsl_ctx.current_scope->first_instr = instr;
                            } else {
                                hlsl_ctx.current_scope->last_instr->next = instr;
                            }
                            hlsl_ctx.current_scope->last_instr = instr;
                        }
                        | if_else_construct
                        | opt_attribute KW_WHILE '(' expression ')' '{' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block '}'
                        | KW_DO '{' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block '}' KW_WHILE '(' expression ')' ';'
                        | opt_attribute KW_FOR '(' expression ';'  expression ';' expression ')' '{' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block '}'
                        | opt_attribute KW_SWITCH '(' expression ')' '{' switch_blocks '}'
                        | KW_BREAK ';'
                        | KW_CONTINUE ';'
                        | KW_DISCARD ';'

switch_blocks:          switch_block | switch_block switch_blocks

switch_block:           KW_CASE INTEGER ':' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block
                        | KW_DEFAULT ':' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block

if_else_construct:      opt_attribute if_statement
                        | opt_attribute if_statement KW_ELSE '{' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block '}'
                        | opt_attribute if_statement KW_ELSE if_else_construct

if_statement:           KW_IF '(' expression ')' '{' { hlsl_push_scope(&hlsl_ctx.current_scope, hlsl_ctx.target); } statement_block '}'

opt_attribute:          /* empty */
                        | '[' KW_ALLOW_UAV_CONDITION ']'
                        | '[' KW_BRANCH ']'
                        | '[' KW_CALL ']'
                        | '[' KW_FASTOPT ']'
                        | '[' KW_FLATTEN ']'
                        | '[' KW_FORCECASE ']'
                        | '[' KW_LOOP ']'
                        | '[' KW_UNROLL '(' expression ')' ']'

return_value:           /* empty */
                        {
                            $$ = NULL;
                        }
                        | expression
                        {
                            $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_parameter));
                            $$->next = NULL;
                            $$->value = $1;
                            /*if($1->value_type == HLSL_VALUE_IMMEDIATE) {
                                struct hlsl_data_type *data_type = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
                                struct hlsl_parameter *param = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(struct hlsl_parameter));
                                param->value = $1;
                                $$ = HeapAlloc(GetProcessHeap(), 0, sizeof(struct shader_reg));
                                if(!hlsl_ctx.current_scope->return_type)
                                    ERR("Returning from scope that has no return type set.\n");
                                CopyMemory(data_type, hlsl_ctx.current_scope->return_type, sizeof(struct hlsl_data_type));
                                hlsl_convert_values(param, data_type);
                                hlsl_declare_constant(&hlsl_ctx, data_type, $$);
                            } else if($1->value_type == HLSL_VALUE_REGISTER) {
                                $$ = $1->value.reg;
                            } else {
                                ERR("Invalid return value\n");
                                $$ = NULL;
                            }*/
                        }

%%

void hlslparser_error (char const *s) {
    printf("Line %u: Error \"%s\" (%u) from bison\n", hlsl_ctx.line_no, s, yylex());
    hlslparser_message(&hlsl_ctx, "Line %u: Error \"%s\" (%u) from bison\n", hlsl_ctx.line_no, s, yylex());
    hlsl_set_parse_status(&hlsl_ctx, PARSE_ERR);
}

/* Error reporting function */
void hlslparser_message(struct hlsl_parser *ctx, const char *fmt, ...) {
    va_list args;
    char* newbuffer;
    int rc, newsize;

    if(ctx->messagecapacity == 0) {
        ctx->messages = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MESSAGEBUFFER_INITIAL_SIZE);
        if(ctx->messages == NULL) {
            ERR("Error allocating memory for parser messages\n");
            return;
        }
        ctx->messagecapacity = MESSAGEBUFFER_INITIAL_SIZE;
    }

    while(1) {
        va_start(args, fmt);
        rc = vsnprintf(ctx->messages + ctx->messagesize,
                       ctx->messagecapacity - ctx->messagesize, fmt, args);
        va_end(args);

        if (rc < 0 ||                                           /* C89 */
            rc >= ctx->messagecapacity - ctx->messagesize) {    /* C99 */
            /* Resize the buffer */
            newsize = ctx->messagecapacity * 2;
            newbuffer = HeapReAlloc(GetProcessHeap(), 0, ctx->messages, newsize);
            if(newbuffer == NULL){
                ERR("Error reallocating memory for parser messages\n");
                return;
            }
            ctx->messages = newbuffer;
            ctx->messagecapacity = newsize;
        } else {
            ctx->messagesize += rc;
            return;
        }
    }
}

/* New status is the worst between current status and parameter value */
void hlsl_set_parse_status(struct hlsl_parser *ctx, enum parse_status status) {
    if(status == PARSE_ERR) ctx->status = PARSE_ERR;
    else if(status == PARSE_WARN && ctx->status == PARSE_SUCCESS) ctx->status = PARSE_WARN;
}

char *debugstr_data_type(struct hlsl_data_type *dt) {
    int i;
    char *buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 128 * sizeof(char));
    switch(dt->type) {
        case HLSL_SCALAR_BOOL:
            sprintf(buffer, "bool%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_DOUBLE:
            sprintf(buffer, "double%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_FLOAT:
            sprintf(buffer, "float%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_HALF:
            sprintf(buffer, "half%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_INT:
            sprintf(buffer, "int%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_STRING:
            sprintf(buffer, "string%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_SCALAR_UINT:
            sprintf(buffer, "uint%dx%d", dt->rows, dt->cols);
            break;
        case HLSL_STRUCT:
            sprintf(buffer, "struct (");
            for(i = 0; i < dt->value.structure.count; i++) {
                sprintf(buffer, "%s %s(%s)", buffer, dt->value.structure.members[i].name, debugstr_data_type(dt->value.structure.members[i].data_type));
            }
            sprintf(buffer, "%s )", buffer);
            break;
        default:
            sprintf(buffer, "Invalid data type!");
            break;
    }
    return buffer;
}

char *debugstr_params(struct hlsl_parameter *params) {
    char *buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 128 * sizeof(char));
    BOOL first = TRUE;
    while(params != NULL) {
        if(first) {
            sprintf(buffer, "%s", debugstr_expression(params->value));
            first = FALSE;
        } else {
            sprintf(buffer, "%s, %s", buffer, debugstr_expression(params->value));
        }
        params = params->next;
    }
    return buffer;
}

char *debugstr_expression(struct hlsl_expression *expr) {
    char *buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 128 * sizeof(char));
    if(!expr) {
        sprintf(buffer, "(nil)");
        return buffer;
    }
    switch(expr->op) {
        case HLSL_OP_VAR:
            /* Fixme: display structs correctly */
            sprintf(buffer, "%s", expr->A.var->name);
            break;
        case HLSL_OP_ASSIGN:
            /* Fixme: display structs correctly */
            sprintf(buffer, "%s = %s", expr->A.var->name, debugstr_expression(expr->B.expr));
            break;
        case HLSL_OP_INTRINSIC_FUNC:
            sprintf(buffer, "%s(%s)", debugstr_intrinsic_func(expr->A.func), debugstr_params(expr->B.params));
            break;
        default:
            sprintf(buffer, "Invalid operator.");
            break;
    }
    return buffer;
}

const char *debugstr_intrinsic_func(enum hlsl_intrinsic_functions func) {
    switch(func) {
        case F_MUL: return "mul";
        case F_DOT: return "dot";
        default:    return "Invalid";
    }
}

void hlslparser_print_intermediate(struct hlsl_parser *hlsl_ctx) {
    unsigned int i, j;
    struct hlsl_instruction *instr;
    TRACE_(hlsl_parsed)("Main function: %s\n", hlsl_ctx->main_func);
    TRACE_(hlsl_parsed)("Target: %s\n", hlsl_ctx->target);
    TRACE_(hlsl_parsed)("Messages: %s\n", hlsl_ctx->messages);
    TRACE_(hlsl_parsed)("Global vars (%u):\n", hlsl_ctx->current_scope->var_count);
    for(i = 0; i < hlsl_ctx->current_scope->var_count; i++) {
        TRACE_(hlsl_parsed)("(%u) -> (%s) %s\n", i + 1, debugstr_data_type(hlsl_ctx->current_scope->vars[i]->data_type), hlsl_ctx->current_scope->vars[i]->name);
    }
    TRACE_(hlsl_parsed)("Custom types (%u):\n", hlsl_ctx->current_scope->custom_type_count);
    for(i = 0; i < hlsl_ctx->current_scope->custom_type_count; i++) {
        TRACE_(hlsl_parsed)("(%u) -> (%s) %s\n", i + 1, debugstr_data_type(hlsl_ctx->current_scope->custom_types[i]->value), hlsl_ctx->current_scope->custom_types[i]->custom_name);
    }
    TRACE_(hlsl_parsed)("Functions (%u):\n", hlsl_ctx->function_count);
    for(i = 0; i < hlsl_ctx->function_count; i++) {
        TRACE_(hlsl_parsed)("(%u) -> (%s) %s\n", i + 1, debugstr_data_type(hlsl_ctx->functions[i]->return_type), hlsl_ctx->functions[i]->name);
        TRACE_(hlsl_parsed)(" -> Function vars (%u):\n", hlsl_ctx->functions[i]->scope->var_count);
        for(j = 0; j < hlsl_ctx->functions[i]->scope->var_count; j++) {
            TRACE_(hlsl_parsed)("    (%u) -> (%s) %s\n", j + 1, debugstr_data_type(hlsl_ctx->functions[i]->scope->vars[j]->data_type), hlsl_ctx->functions[i]->scope->vars[j]->name);
        }
        TRACE_(hlsl_parsed)(" -> Custom types (%u):\n", hlsl_ctx->functions[i]->scope->custom_type_count);
        for(j = 0; j < hlsl_ctx->functions[i]->scope->custom_type_count; j++) {
            TRACE_(hlsl_parsed)("    (%u) -> (%s) %s\n", j + 1, debugstr_data_type(hlsl_ctx->functions[i]->scope->custom_types[j]->value), hlsl_ctx->functions[j]->scope->custom_types[i]->custom_name);
        }
        TRACE_(hlsl_parsed)(" -> Body:\n");
        instr = hlsl_ctx->functions[i]->scope->first_instr;
        while(instr != NULL) {
            if(instr->type == HLSL_INSTR_STATEMENT) {
                TRACE_(hlsl_parsed)("    %s;\n", debugstr_expression(instr->args->value));
            } else if(instr->type == HLSL_INSTR_RETURN) {
                TRACE_(hlsl_parsed)("    return %s;\n", debugstr_expression(instr->args->value));
            } else {
                TRACE_(hlsl_parsed)("    Invalid instruction\n");
            }
            instr = instr->next;
        }
    }
}

struct bwriter_shader *parse_hlsl_shader(char **messages, const char *main_func, const char *target) {
    struct bwriter_shader *ret = NULL;
    struct hlsl_data_type value;
    int i;

    hlsl_ctx.shader = NULL;
    hlsl_ctx.status = PARSE_SUCCESS;
    hlsl_ctx.messagesize = hlsl_ctx.messagecapacity = 0;
    hlsl_ctx.line_no = 1;
    hlsl_ctx.main_func = main_func;
    hlsl_ctx.target = target;
    hlsl_ctx.current_scope = NULL;
    hlsl_push_scope(&hlsl_ctx.current_scope, target);
    hlsl_ctx.constant_count = hlsl_ctx.output_count = hlsl_ctx.input_count = hlsl_ctx.temp_count = hlsl_ctx.output_decl_count = 0;
    hlsl_ctx.constants = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct float4 *));
    hlsl_ctx.inputs = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct hlsl_decl *));
    hlsl_ctx.output_decl = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct hlsl_decl));
    hlsl_ctx.functions = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct hlsl_function*));
    for(i = 0; i < 100; i++) {
        hlsl_ctx.functions[i] = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(struct hlsl_function));
    }

    value.type = HLSL_SCALAR_FLOAT;
    value.rows = 1;
    value.cols = 1;
    value.value_type = HLSL_VALUE_INVALID;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "float1", value); value.cols++;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "float2", value); value.cols++;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "float3", value); value.cols++;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "float4", value); value.type = HLSL_SCALAR_INT;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "int4", value);value.cols = 2;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "int2", value);value.cols = value.rows = 4; value.type = HLSL_SCALAR_FLOAT;
    hlsl_add_custom_type(hlsl_ctx.current_scope, "float4x4", value);

    hlslparser_parse();

    if(TRACE_ON(hlsl_parsed)) hlslparser_print_intermediate(&hlsl_ctx);

    if(hlsl_ctx.status != PARSE_ERR) {
        hlsl_compile(&hlsl_ctx);
    } else {
        ERR("Parsing of HLSL shader failed!\n");
    }
    /*for(i = 0; i < hlsl_ctx.constant_count; i++) {*/
        /*hlsl_ctx.current_scope->asm_ctx.funcs->constF(&hlsl_ctx.current_scope->asm_ctx, i, hlsl_ctx.constants[i]->x.v, hlsl_ctx.constants[i]->y.v, hlsl_ctx.constants[i]->z.v, hlsl_ctx.constants[i]->w.v);*/
        /*INTERMEDIATE*/
    /*}*/

    //ret = hlsl_ctx.current_scope->asm_ctx.shader;
    /*INTERMEDIATE*/
    //ret = merge_bwriter_shaders(hlsl_ctx.current_scope->asm_ctx.shader, hlsl_ctx.functions[0]->compiled_code);

    //hlsl_ctx.current_scope->asm_ctx.funcs->end(&hlsl_ctx.current_scope->asm_ctx);
    /*INTERMEDIATE*/
//    if(hlsl_ctx.status != PARSE_ERR) ret = hlsl_ctx.current_scope->asm_ctx.shader;
//    else if(hlsl_ctx.shader) /*SlDeleteShader(hlsl_ctx.shader);*/ {}

    if(messages) {
        if(hlsl_ctx.messagesize) {
            /* Shrink the buffer to the used size */
            *messages = HeapReAlloc(GetProcessHeap(), 0, hlsl_ctx.messages, hlsl_ctx.messagesize + 1);
            if(!*messages) {
                ERR("Out of memory, no messages reported\n");
                HeapFree(GetProcessHeap(), 0, hlsl_ctx.messages);
            }
        } else {
            *messages = NULL;
        }
    } else {
        if(hlsl_ctx.messagecapacity) HeapFree(GetProcessHeap(), 0, hlsl_ctx.messages);
    }

    return ret;
}