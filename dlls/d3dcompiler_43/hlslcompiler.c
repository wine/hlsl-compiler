/*
* Direct3D HLSL compiler
*
* Copyright 2010 Matijn Woudt
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
*/


#include "config.h"
#include "wine/port.h"
#include "wine/debug.h"

#include "hlsl.h"

WINE_DEFAULT_DEBUG_CHANNEL(d3dcompiler);

void hlsl_compile(struct hlsl_parser *hlsl_ctx) {
    struct hlsl_function *func;
    struct hlsl_instruction *instr;
    struct asm_parser asm_ctx;
    struct shader_reg *return_reg;

    func = hlsl_find_function(hlsl_ctx, hlsl_ctx->main_func);
    if(!func) {
        ERR("Failed to find main function\n");
        hlsl_ctx->status = PARSE_ERR;
        return;
    }

    asm_ctx.shader = NULL;
    asm_ctx.status = PARSE_SUCCESS;
    asm_ctx.messagesize = asm_ctx.messagecapacity = 0;
    asm_ctx.line_no = 1;

    if(!lstrcmpA(hlsl_ctx->target, "vs_2_0"))
        create_vs20_parser(&asm_ctx);
    else if(!lstrcmpA(hlsl_ctx->target, "vs_3_0"))
        create_vs30_parser(&asm_ctx);
    else {
        FIXME("Unsupported target\n");
        hlsl_ctx->status = PARSE_ERR;
        return;
    }

    instr = func->scope->first_instr;
    while(instr != NULL) {
        if(func->return_type->value_type == HLSL_VALUE_SEMANTIC) {
            return_reg = HeapAlloc(GetProcessHeap(), 0, sizeof(struct shader_reg));
            hlsl_declare_output(hlsl_ctx, &asm_ctx, func->return_type->value.sem, return_reg);
        }  else {
            return_reg = HeapAlloc(GetProcessHeap(), 0, sizeof(struct shader_reg));
            return_reg->type = BWRITERSPR_OUTPUT;
            return_reg->regnum = hlsl_ctx->temp_count++; /* FIXME: keep track of used temp registers */
            return_reg->writemask = BWRITERSP_WRITEMASK_ALL;
            return_reg->srcmod = 0;
            return_reg->rel_reg = NULL;
        }
        hlsl_compile_instruction(hlsl_ctx, &asm_ctx, func->scope, return_reg, instr);
        instr = instr->next;
    }
}

struct hlsl_function *hlsl_find_function(struct hlsl_parser *hlsl_ctx, const char *name) {
    int i;
    for(i = 0; i < hlsl_ctx->function_count; i++) {
        if(!lstrcmpA(hlsl_ctx->functions[i]->name, name)) {
            return hlsl_ctx->functions[i];
        }
    }
    return NULL;
}

void hlsl_compile_instruction(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_scope *scope, struct shader_reg *return_reg, struct hlsl_instruction *instr) {
    /* Compile instruction */
    struct src_regs src;
    switch(instr->type) {
        case HLSL_INSTR_STATEMENT:
            TRACE("Statement\n");
            break;
        case HLSL_INSTR_RETURN:
            TRACE("Return instruction\n");
            src.count = 1;
            if(!hlsl_compile_expression(hlsl_ctx, asm_ctx, scope, instr->args->value, &src.reg[0])) ERR("Failed to compile expression\n");
            asm_ctx->funcs->instr(asm_ctx, BWRITERSIO_MOV, 0, 0, 0, return_reg, &src, 1);
            break;
        case HLSL_INSTR_IF:
            TRACE("If statement\n");
            break;
        case HLSL_INSTR_LOOP:
            TRACE("Loop statement\n");
            break;
        default:
            ERR("Invalid instruction type, this should never happen!\n");
    }
}

BOOL hlsl_compile_expression(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_scope *scope, struct hlsl_expression *expr, struct shader_reg *reg) {
    switch(expr->op) {
        case HLSL_OP_VAR:
            if(expr->A.var->data_type->value_type == HLSL_VALUE_SEMANTIC) {
                hlsl_declare_input(hlsl_ctx, asm_ctx, expr->A.var->data_type->value.sem, reg);
                return TRUE;
            } else {
                FIXME("TODO\n");
                return FALSE;
            }
            break;
        default:
            ERR("Invalid expression opcode (%d), this should never happen!\n", expr->op);
            reg = NULL;
            return FALSE;
    }
}

void hlsl_declare_input(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_semantic *semantic, struct shader_reg *reg) {
    int i;

    for(i = 0; i < hlsl_ctx->input_count; i++) {
        if(hlsl_ctx->inputs[i]->sem->semantic == semantic->semantic && hlsl_ctx->inputs[i]->sem->num == semantic->num) {
            TRACE("Input already declared, ret: %d (index %d)\n", hlsl_ctx->inputs[i]->reg->regnum, i);
            CopyMemory(reg, hlsl_ctx->inputs[i]->reg, sizeof(struct shader_reg));
        }
    }

    reg->type = BWRITERSPR_INPUT;
    reg->regnum = hlsl_ctx->input_count;
    reg->swizzle = BWRITERVS_NOSWIZZLE;
    reg->srcmod = 0;
    reg->rel_reg = NULL;
    asm_ctx->funcs->dcl_input(asm_ctx, semantic->semantic, semantic->num, 0, reg);
    hlsl_ctx->inputs[hlsl_ctx->input_count] = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_decl));
    hlsl_ctx->inputs[hlsl_ctx->input_count]->reg = reg;
    hlsl_ctx->inputs[hlsl_ctx->input_count++]->sem = semantic;
}

void hlsl_declare_output(struct hlsl_parser *hlsl_ctx, struct asm_parser *asm_ctx, struct hlsl_semantic *semantic, struct shader_reg *reg) {
    TRACE("%p, %p, (%d, %d), %p)\n", hlsl_ctx, asm_ctx, semantic->semantic, semantic->num, reg);

    reg->type = BWRITERSPR_OUTPUT;
    reg->regnum = hlsl_ctx->output_count++;
    reg->writemask = BWRITERSP_WRITEMASK_ALL;
    reg->srcmod = 0;
    asm_ctx->funcs->dcl_output(asm_ctx, semantic->semantic, semantic->num, reg);
}
