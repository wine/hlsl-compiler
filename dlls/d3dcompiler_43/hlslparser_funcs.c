/*
* Direct3D HLSL compiler
*
* Copyright 2010 Matijn Woudt
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
*/

#include "config.h"
#include "wine/port.h"
#include "wine/debug.h"

#include "hlsl.h"

WINE_DEFAULT_DEBUG_CHANNEL(d3dcompiler);

/* Lexer and parser helper functions */

BOOL hlsl_get_custom_type(struct hlsl_scope *scope, const char *name, struct hlsl_data_type **value) {
    int i;
    TRACE("Trying to find custom type %s in scope %p\n", name, scope);
    for(i = 0; i < scope->custom_type_count; i++) {
        if(!lstrcmpiA(name, scope->custom_types[i]->custom_name)) {
            *value = scope->custom_types[i]->value;
            return TRUE;
        }
    }
    if(scope->upper) {
        return hlsl_get_custom_type(scope->upper, name, value);
    }
    return FALSE;
}

BOOL hlsl_add_custom_type(struct hlsl_scope *scope, const char *name, struct hlsl_data_type value) {
    struct hlsl_data_type *val = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_data_type));
    CopyMemory(val, &value, sizeof(struct hlsl_data_type));
    scope->custom_types[scope->custom_type_count]->custom_name = name;
    scope->custom_types[scope->custom_type_count++]->value = val;
    return TRUE;
}

BOOL hlsl_get_variable(struct hlsl_scope *scope, char *name, struct hlsl_variable **value) {
    char *res;
    int i;
    TRACE("Trying to find variable %s in scope %p\n", name, scope);
    res = strchr(name, '.');
    if(res) {
        res[0] = 0;
        TRACE("Trying to find %s in %s\n", res+1, name);
        if(!hlsl_get_variable(scope, name, value)) return FALSE;
        return hlsl_find_var_in_struct(scope, res+1, value);
    }
    for(i = 0; i < scope->var_count; i++) {
        if(!lstrcmpiA(name, scope->vars[i]->name)) {
            *value = scope->vars[i];
            return TRUE;
        }
    }
    if(scope->upper) {
        return hlsl_get_variable(scope->upper, name, value);
    }
    return FALSE;
}

BOOL hlsl_find_var_in_struct(struct hlsl_scope *scope, const char *name, struct hlsl_variable **value) {
    if((*value)->data_type->type == HLSL_STRUCT) {
        int i;
        for(i = 0; i < (*value)->data_type->value.structure.count; i++) {
            if(!lstrcmpiA(name, (*value)->data_type->value.structure.members[i].name)) {
                (*value) =  &(*value)->data_type->value.structure.members[i];
                return TRUE;
            }
        }
    }
    return FALSE;
}

void hlsl_push_scope(struct hlsl_scope **old, const char *target) {
    int i;
    struct hlsl_scope *new = HeapAlloc(GetProcessHeap(), 0, sizeof(struct hlsl_scope));
    new->upper = *old;
    new->var_count = new->custom_type_count = 0;
    new->vars = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct _D3DXCONSTANT_DESC*));
    new->custom_types = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 100 * sizeof(struct hlsl_custom_type*));
    for(i = 0; i < 100; i++) {
        new->vars[i] = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(struct _D3DXCONSTANT_DESC));
        new->custom_types[i] = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(struct hlsl_custom_type));
    }
    
    *old = new;
}

void hlsl_pop_scope(struct hlsl_scope **old) {
    /* TODO: Freeing memory? */
    *old = (*old)->upper;
}
