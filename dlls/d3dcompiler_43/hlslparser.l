/*
 * Direct3D HLSL compiler
 *
 * Copyright 2010 Matijn Woudt
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%{
#include "config.h"
#include "wine/port.h"
#include "wine/debug.h"

#include "hlsl.h"
#include "hlslparser.tab.h"

WINE_DEFAULT_DEBUG_CHANNEL(d3dcompiler);

%}

%option case-insensitive
%option noyywrap
%option prefix="hlslparser_"
%option noinput nounput

PREPROCESSORDIRECTIVE   #[^\n]*\n

/* Comments */
DOUBLESLASHCOMMENT      "//"[^\n]*
MULTILINECOMMENT        "/*""/"*([^*/]|[^*]"/"|"*"[^/])*"*"*"*/"

/* Whitespaces are spaces, tabs and newlines */
WHITESPACE              [ \t]+
NEWLINE                 (\n)|(\r\n)

DOT                     "."
COMMA                   ","
LPAREN                  \(
RPAREN                  \)
SEMICOLON               ;
LCBRACKET               \{
RCBRACKET               \}
LBRACKET                \[
RBRACKET                \]
EQUALS                  \=
LEFT                    \<
RIGHT                   \>
MUL                     \*
PLUS                    \+
MINUS                   \-
DIV                     \/
MOD                     \%
NOT                     \!
AND                     \&
OR                      \|
QMARK                   \?
COLON                   \:
BITWNOT                 \~
XOR                     \^

SYMBOLS                 {DOT}|{COMMA}|{LPAREN}|{RPAREN}|{SEMICOLON}|{LCBRACKET}|{RCBRACKET}|{LBRACKET}|{RBRACKET}|{EQUALS}|{LEFT}|{RIGHT}|{MUL}|{PLUS}|{MINUS}|{DIV}|{MOD}|{NOT}|{AND}|{OR}|{QMARK}|{COLON}|{BITWNOT}|{XOR}

DECIMAL                 [1-9][0-9]*[uUlL]?
OCTAL                   0[0-9]*[uUlL]?
HEXADECIMAL             0x[0-9]+[uUlL]?

FLOAT                   (([0-9]*\.[0-9]+)|([0-9]+\.))([eE](\+|-)?[0-9]+)?[hHfF]?

IDENTIFIER              [a-zA-Z_][a-zA-Z0-9_]*(\.[a-zA-Z_][a-zA-Z0-9_]*)*

ANY                     (.)

%%

allow_uav_condition             {   return KW_ALLOW_UAV_CONDITION; }
:{WHITESPACE}*binormal[0-9]?    {   hlslparser_lval.integer = yytext[yyleng-1]-'0'; return KW_BINORMAL; }
:{WHITESPACE}*blendindices[0-9]? {  hlslparser_lval.integer = yytext[yyleng-1]-'0'; return KW_BLENDINDICES; }
blendstate                      {   return KW_BLENDSTATE; }
:{WHITESPACE}*blendweight[0-9]? {   hlslparser_lval.integer = yytext[yyleng-1]-'0'; return KW_BLENDWEIGHT; }
bool                            {   return KW_BOOL; }
branch                          {   return KW_BRANCH; }
break                           {   return KW_BREAK; }
buffer                          {   return KW_BUFFER; }
call                            {   return KW_CALL; }
case                            {   return KW_CASE; }
cbuffer                         {   return KW_CBUFFER; }
centroid                        {   return KW_CENTROID; }
class                           {   return KW_CLASS; }
:{WHITESPACE}*color[0-9]?       {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_COLOR; }
column_major                    {   return KW_COLUMN_MAJOR; }
compile                         {   return KW_COMPILE; }
const                           {   return KW_CONST; }
continue                        {   return KW_CONTINUE; }
default                         {   return KW_DEFAULT; }
:{WHITESPACE}*depth[0-9]?       {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_DEPTH; }
depthstencilstate               {   return KW_DEPTHSTENCILSTATE; }
depthstencilview                {   return KW_DEPTHSTENCILVIEW; }
discard                         {   return KW_DISCARD; }
do                              {   return KW_DO; }
double                          {   return KW_DOUBLE; }
else                            {   return KW_ELSE; }
extern                          {   return KW_EXTERN; }
false                           {   hlslparser_lval.integer = 0; return INTEGER; }
fastopt                         {   return KW_FASTOPT; }
flatten                         {   return KW_FLATTEN; }
float                           {   return KW_FLOAT; }
:{WHITESPACE}*fog               {   return KW_FOG; }
for                             {   return KW_FOR; }
forcecase                       {   return KW_FORCECASE; }
geometryshader                  {   return KW_GEOMETRYSHADER; }
groupshared                     {   return KW_GROUPSHARED; }
half                            {   return KW_HALF; }
if                              {   return KW_IF; }
in                              {   return KW_IN; }
inline                          {   return KW_INLINE; }
inout                           {   return KW_INOUT; }
int                             {   return KW_INT; }
interface                       {   return KW_INTERFACE; }
linear                          {   return KW_LINEAR; }
loop                            {   return KW_LOOP; }
matrix                          {   return KW_MATRIX; }
namespace                       {   return KW_NAMESPACE; }
nointerpolation                 {   return KW_NOINTERPOLATION; }
noperspective                   {   return KW_NOPERSPECTIVE; }
:{WHITESPACE}*normal[0-9]?      {   hlslparser_lval.integer = yytext[yyleng-1]-'0'; return KW_NORMAL; }
out                             {   return KW_OUT; }
:{WHITESPACE}*packoffset        {   return KW_PACKOFFSET; }
pass                            {   return KW_PASS; }
pixelshader                     {   return KW_PIXELSHADER; }
:{WHITESPACE}*position[0-9]?    {   if(yytext[yyleng-1] >= '0' && yytext[yyleng-1] <= '9') hlslparser_lval.integer = yytext[yyleng-1]-'0'; else hlslparser_lval.integer = 0; return KW_POSITION; }
:{WHITESPACE}*positiont         {   return KW_POSITIONT; }
precise                         {   return KW_PRECISE; }
:{WHITESPACE}*psize[0-9]?       {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_PSIZE; }
rasterizerstate                 {   return KW_RASTERIZERSTATE; }
:{WHITESPACE}*register          {   return KW_REGISTER; }
rendertargetview                {   return KW_RENDERTARGETVIEW; }
return                          {   return KW_RETURN; }
row_major                       {   return KW_ROW_MAJOR; }
sample                          {   return KW_SAMPLE; }
sampler                         {   return KW_SAMPLER; }
sampler1d                       {   return KW_SAMPLER1D; }
sampler2d                       {   return KW_SAMPLER2D; }
sampler3d                       {   return KW_SAMPLER3D; }
samplercube                     {   return KW_SAMPLERCUBE; }
sampler_state                   {   return KW_SAMPLER_STATE; }
samplercomparisonstate          {   return KW_SAMPLERCOMPARISONSTATE; }
shared                          {   return KW_SHARED; }
stateblock                      {   return KW_STATEBLOCK; }
stateblock_state                {   return KW_STATEBLOCK_STATE; }
static                          {   return KW_STATIC; }
string                          {   return KW_STRING; }
struct                          {   return KW_STRUCT; }
switch                          {   return KW_SWITCH; }
:{WHITESPACE}*tangent[0-9]?     {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_TANGENT; }
tbuffer                         {   return KW_TBUFFER; }
technique                       {   return KW_TECHNIQUE; }
technique10                     {   return KW_TECHNIQUE10; }
:{WHITESPACE}*tessfactor[0-9]?  {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_TESSFACTOR; }
:{WHITESPACE}*texcoord[0-9]?    {   hlslparser_lval.integer = yytext[yyleng]-'0'; return KW_TEXCOORD; }
texture                         {   return KW_TEXTURE; }
texture1d                       {   return KW_TEXTURE1D; }
texture1darray                  {   return KW_TEXTURE1DARRAY; }
texture2d                       {   return KW_TEXTURE2D; }
texture2darray                  {   return KW_TEXTURE2DARRAY; }
texture2dms                     {   return KW_TEXTURE2DMS; }
texture2dmsarray                {   return KW_TEXTURE2DMSARRAY; }
texture3d                       {   return KW_TEXTURE3D; }
texturecube                     {   return KW_TEXTURECUBE; }
texturecubearray                {   return KW_TEXTURECUBEARRAY; }
true                            {   hlslparser_lval.integer = 1; return INTEGER; }
typedef                         {   return KW_TYPEDEF; }
uint                            {   return KW_UINT; }
uniform                         {   return KW_UNIFORM; }
unroll                          {   return KW_UNROLL; }
vector                          {   return KW_VECTOR; }
vertexshader                    {   return KW_VERTEXSHADER; }
:{WHITESPACE}*vface             {   return KW_VFACE; }
void                            {   return KW_VOID; }
volatile                        {   return KW_VOLATILE; }
:{WHITESPACE}*vpos              {   return KW_VPOS; }
while                           {   return KW_WHILE; }

abs                             {   hlslparser_lval.func = F_ABS; return FUNCTION; }
acos                            {   hlslparser_lval.func = F_ACOS; return FUNCTION; }
all                             {   hlslparser_lval.func = F_ALL; return FUNCTION; }
AllMemoryBarrier                {   hlslparser_lval.func = F_ALLMEMORYBARRIER; return FUNCTION; }
AllMemoryBarrierWithGroupSync   {   hlslparser_lval.func = F_ALLMEMORYBARRIERWITHGROUPSYNC; return FUNCTION; }
any                             {   hlslparser_lval.func = F_ANY; return FUNCTION; }
asdouble                        {   hlslparser_lval.func = F_ASDOUBLE; return FUNCTION; }
asfloat                         {   hlslparser_lval.func = F_ASFLOAT; return FUNCTION; }
asin                            {   hlslparser_lval.func = F_ASIN; return FUNCTION; }
asint                           {   hlslparser_lval.func = F_ASINT; return FUNCTION; }
asuint                          {   hlslparser_lval.func = F_ASUINT; return FUNCTION; }
atan                            {   hlslparser_lval.func = F_ATAN; return FUNCTION; }
atan2                           {   hlslparser_lval.func = F_ATAN2; return FUNCTION; }
ceil                            {   hlslparser_lval.func = F_CEIL; return FUNCTION; }
clamp                           {   hlslparser_lval.func = F_CLAMP; return FUNCTION; }
clip                            {   hlslparser_lval.func = F_CLIP; return FUNCTION; }
cos                             {   hlslparser_lval.func = F_COS; return FUNCTION; }
cosh                            {   hlslparser_lval.func = F_COSH; return FUNCTION; }
countbits                       {   hlslparser_lval.func = F_COUNTBITS; return FUNCTION; }
cross                           {   hlslparser_lval.func = F_CROSS; return FUNCTION; }
D3DCOLORtoUBYTE4                {   hlslparser_lval.func = F_D3DCOLORTOUBYTE4; return FUNCTION; }
ddx                             {   hlslparser_lval.func = F_DDX; return FUNCTION; }
ddx_coarse                      {   hlslparser_lval.func = F_DDX_COARSE; return FUNCTION; }
ddx_fine                        {   hlslparser_lval.func = F_DDX_FINE; return FUNCTION; }
ddy                             {   hlslparser_lval.func = F_DDY; return FUNCTION; }
ddy_coarse                      {   hlslparser_lval.func = F_DDY_COARSE; return FUNCTION; }
ddy_fine                        {   hlslparser_lval.func = F_DDY_FINE; return FUNCTION; }
degrees                         {   hlslparser_lval.func = F_DEGREES; return FUNCTION; }
determinant                     {   hlslparser_lval.func = F_DETERMINANT; return FUNCTION; }
DeviceMemoryBarrier             {   hlslparser_lval.func = F_DEVICEMEMORYBARRIER; return FUNCTION; }
DeviceMemoryBarrierWithGroupSync {  hlslparser_lval.func = F_DEVICEMEMORYBARRIERWITHGROUPSYNC; return FUNCTION; }
distance                        {   hlslparser_lval.func = F_DISTANCE; return FUNCTION; }
dot                             {   hlslparser_lval.func = F_DOT; return FUNCTION; }
dst                             {   hlslparser_lval.func = F_DST; return FUNCTION; }
EvaluateAttributeAtCentroid     {   hlslparser_lval.func = F_EVALUATEATTRIBUTEATCENTROID; return FUNCTION; }
EvaluateAttributeAtSample       {   hlslparser_lval.func = F_EVALUATEATTRIBUTEATSAMPLE; return FUNCTION; }
EvaluateAttributeSnapped        {   hlslparser_lval.func = F_EVALUATEATTRIBUTESNAPPED; return FUNCTION; }
exp                             {   hlslparser_lval.func = F_EXP; return FUNCTION; }
exp2                            {   hlslparser_lval.func = F_EXP2; return FUNCTION; }
f16tof32                        {   hlslparser_lval.func = F_F16TOF32; return FUNCTION; }
f32tof16                        {   hlslparser_lval.func = F_F32TOF16; return FUNCTION; }
faceforward                     {   hlslparser_lval.func = F_FACEFORWARD; return FUNCTION; }
firstbithigh                    {   hlslparser_lval.func = F_FIRSTBITHIGH; return FUNCTION; }
firstbitlow                     {   hlslparser_lval.func = F_FIRSTBITLOW; return FUNCTION; }
floor                           {   hlslparser_lval.func = F_FLOOR; return FUNCTION; }
fmod                            {   hlslparser_lval.func = F_FMOD; return FUNCTION; }
frac                            {   hlslparser_lval.func = F_FRAC; return FUNCTION; }
frexp                           {   hlslparser_lval.func = F_FREXP; return FUNCTION; }
fwidth                          {   hlslparser_lval.func = F_FWIDTH; return FUNCTION; }
GetRenderTargetSampleCount      {   hlslparser_lval.func = F_GETRENDERTARGETSAMPLECOUNT; return FUNCTION; }
GetRenderTargetSamplePosition   {   hlslparser_lval.func = F_GETRENDERTARGETSAMPLEPOSITION; return FUNCTION; }
GroupMemoryBarrier              {   hlslparser_lval.func = F_GROUPMEMORYBARRIER; return FUNCTION; }
GroupMemoryBarrierWithGroupSync {   hlslparser_lval.func = F_GROUPMEMORYBARRIERWITHGROUPSYNC; return FUNCTION; }
InterlockedAdd                  {   hlslparser_lval.func = F_INTERLOCKEDADD; return FUNCTION; }
InterlockedAnd                  {   hlslparser_lval.func = F_INTERLOCKEDAND; return FUNCTION; }
InterlockedCompareExchange      {   hlslparser_lval.func = F_INTERLOCKEDCOMPAREEXCHANGE; return FUNCTION; }
InterlockedCompareStore         {   hlslparser_lval.func = F_INTERLOCKEDCOMPARESTORE; return FUNCTION; }
InterlockedExchange             {   hlslparser_lval.func = F_INTERLOCKEDEXCHANGE; return FUNCTION; }
InterlockedMax                  {   hlslparser_lval.func = F_INTERLOCKEDMAX; return FUNCTION; }
InterlockedMin                  {   hlslparser_lval.func = F_INTERLOCKEDMIN; return FUNCTION; }
InterlockedOr                   {   hlslparser_lval.func = F_INTERLOCKEDOR; return FUNCTION; }
InterlockedXor                  {   hlslparser_lval.func = F_INTERLOCKEDXOR; return FUNCTION; }
isfinite                        {   hlslparser_lval.func = F_ISFINITE; return FUNCTION; }
isinf                           {   hlslparser_lval.func = F_ISINF; return FUNCTION; }
isnan                           {   hlslparser_lval.func = F_ISNAN; return FUNCTION; }
ldexp                           {   hlslparser_lval.func = F_LDEXP; return FUNCTION; }
length                          {   hlslparser_lval.func = F_LENGTH; return FUNCTION; }
lerp                            {   hlslparser_lval.func = F_LERP; return FUNCTION; }
lit                             {   hlslparser_lval.func = F_LIT; return FUNCTION; }
log                             {   hlslparser_lval.func = F_LOG; return FUNCTION; }
log10                           {   hlslparser_lval.func = F_LOG10; return FUNCTION; }
log2                            {   hlslparser_lval.func = F_LOG2; return FUNCTION; }
mad                             {   hlslparser_lval.func = F_MAD; return FUNCTION; }
max                             {   hlslparser_lval.func = F_MAX; return FUNCTION; }
min                             {   hlslparser_lval.func = F_MIN; return FUNCTION; }
modf                            {   hlslparser_lval.func = F_MODF; return FUNCTION; }
mul                             {   hlslparser_lval.func = F_MUL; return FUNCTION; }
noise                           {   hlslparser_lval.func = F_NOISE; return FUNCTION; }
normalize                       {   hlslparser_lval.func = F_NORMALIZE; return FUNCTION; }
pow                             {   hlslparser_lval.func = F_POW; return FUNCTION; }
Process2DQuadTessFactorsAvg     {   hlslparser_lval.func = F_PROCESS2DQUADTESSFACTORSAVG; return FUNCTION; }
Process2DQuadTessFactorsMax     {   hlslparser_lval.func = F_PROCESS2DQUADTESSFACTORSMAX; return FUNCTION; }
Process2DQuadTessFactorsMin     {   hlslparser_lval.func = F_PROCESS2DQUADTESSFACTORSMIN; return FUNCTION; }
ProcessIsolineTessFactors       {   hlslparser_lval.func = F_PROCESSISOLINETESSFACTORS; return FUNCTION; }
ProcessQuadTessFactorsAvg       {   hlslparser_lval.func = F_PROCESSQUADTESSFACTORSAVG; return FUNCTION; }
ProcessQuadTessFactorsMax       {   hlslparser_lval.func = F_PROCESSQUADTESSFACTORSMAX; return FUNCTION; }
ProcessQuadTessFactorsMin       {   hlslparser_lval.func = F_PROCESSQUADTESSFACTORSMIN; return FUNCTION; }
ProcessTriTessFactorsAvg        {   hlslparser_lval.func = F_PROCESSTRITESSFACTORSAVG; return FUNCTION; }
ProcessTriTessFactorsMax        {   hlslparser_lval.func = F_PROCESSTRITESSFACTORSMAX; return FUNCTION; }
ProcessTriTessFactorsMin        {   hlslparser_lval.func = F_PROCESSTRITESSFACTORSMIN; return FUNCTION; }
radians                         {   hlslparser_lval.func = F_RADIANS; return FUNCTION; }
rcp                             {   hlslparser_lval.func = F_RCP; return FUNCTION; }
reflect                         {   hlslparser_lval.func = F_REFLECT; return FUNCTION; }
refract                         {   hlslparser_lval.func = F_REFRACT; return FUNCTION; }
reversebits                     {   hlslparser_lval.func = F_REVERSEBITS; return FUNCTION; }
round                           {   hlslparser_lval.func = F_ROUND; return FUNCTION; }
rsqrt                           {   hlslparser_lval.func = F_RSQRT; return FUNCTION; }
saturate                        {   hlslparser_lval.func = F_SATURATE; return FUNCTION; }
sign                            {   hlslparser_lval.func = F_SIGN; return FUNCTION; }
sin                             {   hlslparser_lval.func = F_SIN; return FUNCTION; }
sincos                          {   hlslparser_lval.func = F_SINCOS; return FUNCTION; }
sinh                            {   hlslparser_lval.func = F_SINH; return FUNCTION; }
smoothstep                      {   hlslparser_lval.func = F_SMOOTHSTEP; return FUNCTION; }
sqrt                            {   hlslparser_lval.func = F_SQRT; return FUNCTION; }
step                            {   hlslparser_lval.func = F_STEP; return FUNCTION; }
tan                             {   hlslparser_lval.func = F_TAN; return FUNCTION; }
tanh                            {   hlslparser_lval.func = F_TANH; return FUNCTION; }
tex1D                           {   hlslparser_lval.func = F_TEX1D; return FUNCTION; }
tex1Dbias                       {   hlslparser_lval.func = F_TEX1DBIAS; return FUNCTION; }
tex1Dgrad                       {   hlslparser_lval.func = F_TEX1DGRAD; return FUNCTION; }
tex1Dlod                        {   hlslparser_lval.func = F_TEX1DLOD; return FUNCTION; }
tex1Dproj                       {   hlslparser_lval.func = F_TEX1DPROJ; return FUNCTION; }
tex2D                           {   hlslparser_lval.func = F_TEX2D; return FUNCTION; }
tex2Dbias                       {   hlslparser_lval.func = F_TEX2DBIAS; return FUNCTION; }
tex2Dgrad                       {   hlslparser_lval.func = F_TEX2DGRAD; return FUNCTION; }
tex2Dlod                        {   hlslparser_lval.func = F_TEX2DLOD; return FUNCTION; }
tex2Dproj                       {   hlslparser_lval.func = F_TEX2DPROJ; return FUNCTION; }
tex3D                           {   hlslparser_lval.func = F_TEX3D; return FUNCTION; }
tex3Dbias                       {   hlslparser_lval.func = F_TEX3DBIAS; return FUNCTION; }
tex3Dgrad                       {   hlslparser_lval.func = F_TEX3DGRAD; return FUNCTION; }
tex3Dlod                        {   hlslparser_lval.func = F_TEX3DLOD; return FUNCTION; }
tex3Dproj                       {   hlslparser_lval.func = F_TEX3DPROJ; return FUNCTION; }
texCUBE                         {   hlslparser_lval.func = F_TEXCUBE; return FUNCTION; }
texCUBEbias                     {   hlslparser_lval.func = F_TEXCUBEBIAS; return FUNCTION; }
texCUBEgrad                     {   hlslparser_lval.func = F_TEXCUBEGRAD; return FUNCTION; }
texCUBElod                      {   hlslparser_lval.func = F_TEXCUBELOD; return FUNCTION; }
texCUBEproj                     {   hlslparser_lval.func = F_TEXCUBEPROJ; return FUNCTION; }
transpose                       {   hlslparser_lval.func = F_TRANSPOSE; return FUNCTION; }
trunc                           {   hlslparser_lval.func = F_TRUNC; return FUNCTION; }

{DECIMAL}                       {   hlslparser_lval.integer = atoi(yytext); return INTEGER; }
{OCTAL}                         {   sscanf(yytext, "%o", &hlslparser_lval.integer); return INTEGER; }
{HEXADECIMAL}                   {   sscanf(yytext, "0x%x", &hlslparser_lval.integer); return INTEGER; }
{FLOAT}                         {   hlslparser_lval.floating = atof(yytext); return FLOATING; }

{EQUALS}                        {   return ASSIGN; }
{PLUS}{EQUALS}                  {   return ASSIGN_ADD; }
{MINUS}{EQUALS}                 {   return ASSIGN_SUB; }
{MUL}{EQUALS}                   {   return ASSIGN_MUL; }
{DIV}{EQUALS}                   {   return ASSIGN_DIV; }
{MOD}{EQUALS}                   {   return ASSIGN_MOD; }
{LEFT}{LEFT}{EQUALS}            {   return ASSIGN_SHIFT_LEFT; }
{RIGHT}{RIGHT}{EQUALS}          {   return ASSIGN_SHIFT_RIGHT; }
{AND}{EQUALS}                   {   return ASSIGN_BITW_AND; }
{XOR}{EQUALS}                   {   return ASSIGN_BITW_XOR; }
{OR}{EQUALS}                    {   return ASSIGN_BITW_OR; }
{EQUALS}{EQUALS}                {   return EQUALS; }
{NOT}{EQUALS}                   {   return NOT_EQUALS; }
{LEFT}{EQUALS}                  {   return LESS_EQUALS; }
{RIGHT}{EQUALS}                 {   return GREATER_EQUALS; }
{AND}{AND}                      {   return BOOLEAN_AND; }
{OR}{OR}                        {   return BOOLEAN_OR; }
{LEFT}{LEFT}                    {   return SHIFT_LEFT; }
{RIGHT}{RIGHT}                  {   return SHIFT_RIGHT; }
{PLUS}{PLUS}                    {   return VAR_INC; }
{MINUS}{MINUS}                  {   return VAR_DEC; }
{SYMBOLS}                       {   TRACE("Line %u: Got symbol: %c\n", hlsl_ctx.line_no,yytext[0]);return yytext[0];           }
{IDENTIFIER}                    {
                                    if(hlsl_get_variable(hlsl_ctx.current_scope, yytext, &hlslparser_lval.variable)) {
                                        TRACE("Line %u: Got variable: %s, %p\n", hlsl_ctx.line_no,yytext, hlslparser_lval.variable);
                                        return VARIABLE;
                                    }
                                    if(hlsl_get_custom_type(hlsl_ctx.current_scope, yytext, &hlslparser_lval.data_type)) {
                                        TRACE("Line %u: Got custom type: %s, %p\n", hlsl_ctx.line_no,yytext, hlslparser_lval.data_type);
                                        return CUSTOM_TYPE;
                                    }
                                    hlslparser_lval.str = strdup(yytext);
                                    TRACE("Line %u: Got ident: %s\n", hlsl_ctx.line_no,yytext);
                                    return IDENTIFIER;
                                }

{PREPROCESSORDIRECTIVE}         {
                                    /* TODO: update current line information */
                                    TRACE("line info update: %s", yytext);
                                }

{DOUBLESLASHCOMMENT}            {                           }
{MULTILINECOMMENT}              {   for(;yyleng > 0;yyleng--) { if(yytext[yyleng] == '\n') hlsl_ctx.line_no++; } }


{WHITESPACE}                    { /* Do nothing */          }
{NEWLINE}                       {
                                    hlsl_ctx.line_no++;
                                }

{ANY}                           {
                                    ERR("Line %u: Unexpected input %s\n", hlsl_ctx.line_no, yytext);
                                    hlsl_set_parse_status(&hlsl_ctx, PARSE_ERR);
                                }

%%

struct bwriter_shader *CompileShader(const char *text, char **messages, const char *main_func, const char *target) {
    struct bwriter_shader *ret = NULL;
    YY_BUFFER_STATE buffer;
    TRACE("%p, %p\n", text, messages);

    buffer = hlslparser__scan_string(text);
    hlslparser__switch_to_buffer(buffer);

    ret = parse_hlsl_shader(messages, main_func, target);

    hlslparser__delete_buffer(buffer);

    return ret;
}